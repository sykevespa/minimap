﻿
var minuuttihavainnot = [];
var minuuttiennuste = [];
var minuuttihavaintojenloppu;


function loadMinuteObservations(minHavTds, callback) {
  let url = 'https://wwwi2.ymparisto.fi/i2/minhavhtml/' + minHavTds + '.js';
  jQuery.ajax({
    url: url,
    dataType: "script",
    timeout: 2000
  }).done(function (script, textStatus) {
            callback();
    })
    .fail(function (jqxhr, settings, exception) {
      console.log('  [ERROR] Loading minuuttihavainnot from ' + url + ' failed: ' + exception);
      callback();
  });     
}

function drawChart(pointname, pointId, variable, targetElementID, lang='fi') {

  // check input
  if(!(pointId && variable && targetElementID)) {
    console.error('[function drawChart]: Failed to create chart, missing parameters');
    return;
  }
  if(!targetElementID.startsWith('#')) {
    targetElementID = '#' + targetElementID;
  }

  var lq = pointId.slice(0,1);
  var va = pointId.slice(1,3);
   
  switch (lang) {

    case 'fi':
      textResources = {
        tw: {
          label: 'pintaveden lämpötila',
          unit: '°C',
          yAxisLabelFormat: '{value: .1f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.1f} °C',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.1f} °C',
          kredit: 'Tulvakeskus / SYKE vesistöennusteet',
          resetZoom: 'Näytä koko vuosi',
          resetZoomTitle: 'Näytä koko vuosi tai valitse alue kuvasta maalaamalla',
        },
        gv: {
          label: 'pohjaveden korkeus',
          unit: 'm (N2000-taso)',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.2f} m (N2000-taso)',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.2f} m',
          kredit: 'Tulvakeskus / SYKE vesistöennusteet',
          resetZoom: 'Näytä koko vuosi',
          resetZoomTitle: 'Näytä koko vuosi tai valitse alue kuvasta maalaamalla',
        },
        q: {
          label: 'virtaama',
          unit: 'm³/s',
          yAxisLabelFormat: '{value: .1f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.1f} m³/s',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.1f} m³/s',
          kredit: 'Tulvakeskus / SYKE vesistöennusteet',
          resetZoom: 'Näytä koko vuosi',
          resetZoomTitle: 'Näytä koko vuosi tai valitse alue kuvasta maalaamalla',
        }, 
        snow: {
          label: 'lumen vesiarvo',
          unit: 'mm',
          yAxisLabelFormat: '{value: .0f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.0f} mm',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.0f} mm',
          kredit: 'Tulvakeskus / SYKE vesistöennusteet',
          resetZoom: 'Näytä koko vuosi',
          resetZoomTitle: 'Näytä koko vuosi tai valitse alue kuvasta maalaamalla',
        }, 
        w: {
          label: 'vedenkorkeus',
          unit: 'm',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.2f} m',
          kredit: 'Tulvakeskus / SYKE vesistöennusteet',
          resetZoom: 'Näytä koko vuosi',
          resetZoomTitle: 'Näytä koko vuosi tai valitse alue kuvasta maalaamalla',
        }, 
        ice: {
          label: 'jään paksuus',
          unit: 'cm',
          yAxisLabelFormat: '{value: .0f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.0f} cm',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.0f} cm',
          kredit: 'Tulvakeskus / SYKE vesistöennusteet',
          resetZoom: 'Näytä koko vuosi',
          resetZoomTitle: 'Näytä koko vuosi tai valitse alue kuvasta maalaamalla',
        },
        sw: {
          label: 'vedenkorkeus',
          unit: 'cm (MW-taso)',
          yAxisLabelFormat: '{value: .0f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.0f} cm',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.0f} cm',
          kredit: 'Tulvakeskus / IL meriennusteet',
          resetZoom: 'Näytä kaksi viikkoa',
          resetZoomTitle: 'Näytä kaksi viikkoa tai valitse alue kuvasta maalaamalla'
        },
        mw: {
          label: 'vedenkorkeus',
          unit: 'cm (MW-taso)',
          yAxisLabelFormat: '{value: .0f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.0f} cm',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.0f} cm',
          kredit: 'Tulvakeskus / IL meriennusteet',
          resetZoom: 'Näytä kaksi viikkoa',
          resetZoomTitle: 'Näytä kaksi viikkoa tai valitse alue kuvasta maalaamalla'
        },
        highestObs: '2000-luvun korkein havainto',
        seriesPointInTime: 'Hetkelliset havainnot',
        observed: 'Havaittu',
        regulationLimit: 'Säännöstelyraja',
        variationLimits: 'Vaihteluväli aiempina vuosina',
        average: 'Keskiarvo aiempina vuosina',
        medianForecast: 'Keskiennuste',
        forecasted: 'Ennustettu'
      };
      Highcharts.setOptions({
        lang: {
          resetZoom: textResources[variable].resetZoom,
          resetZoomTitle: textResources[variable].resetZoomTitle,
          months: ['Tammikuu', 'Helmikuu', 'Maaliskuu', 'Huhtikuu', 'Toukokuu', 'Kesäkuu',  'Heinäkuu', 'Elokuu', 'Syyskuu', 'Lokakuu', 'Marraskuu', 'Joulukuu'],
          shortMonths: ['1', '2', '3', '4', '5', '6',  '7', '8', '9', '10', '11', '12'],
          weekdays: ['Su', 'Ma', 'Ti', 'Ke', 'To', 'Pe', 'La']
        }
      });
    break;

    case 'sv':
      textResources = {
        tw: {
          label: 'ytvattentemperatur',
          unit: '°C',
          yAxisLabelFormat: '{value: .1f}',
          tooltipPointFormatMomentary: '{point.x:%e. %B %Y %H:%M}: {point.y:.1f} °C',
          tooltipPointFormatDay: '{point.x:%e. %B %Y}: {point.y:.1f} °C',
          kredit: 'Översvämningscentret / SYKE vattenprognoser',
          resetZoom: 'Visa hela året',
          resetZoomTitle: 'Visa hela året eller välja genom att måla på grafen'
        },
        gv: {
          label: 'grundvattennivå',
          unit: 'm (N2000-taso)',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %B %Y %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %B %Y}: {point.y:.2f} m (N2000-nivå)',
          kredit: 'Översvämningscentret / SYKE vattenprognoser',
          resetZoom: 'Visa hela året',
          resetZoomTitle: 'Visa hela året eller välja genom att måla på grafen'
        },
        q: {
          label: 'flödet',
          unit: 'm³/s',
          yAxisLabelFormat: '{value: .1f}',
          tooltipPointFormatMomentary: '{point.x:%e. %B %Y %H:%M}: {point.y:.1f} m³/s',
          tooltipPointFormatDay: '{point.x:%e. %B %Y}: {point.y:.1f} m³/s',
          kredit: 'Översvämningscentret / SYKE vattenprognoser',
          resetZoom: 'Visa hela året',
          resetZoomTitle: 'Visa hela året eller välja genom att måla på grafen'
        }, 
        snow: {
          label: 'xxx',
          unit: 'cm',
          yAxisLabelFormat: '{value: .0f}',
          tooltipPointFormatMomentary: '{point.x:%e. %B %Y %H:%M}: {point.y:.0f} mm',
          tooltipPointFormatDay: '{point.x:%e. %B %Y}: {point.y:.0f} mm',
          kredit: 'Översvämningscentret / SYKE vattenprognoser',
          resetZoom: 'Visa hela året',
          resetZoomTitle: 'Visa hela året eller välja genom att måla på grafen'
        }, 
        w: {
          label: 'vattennivån',
          unit: 'm',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %B %Y %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %B %Y}: {point.y:.2f} m (NN-nivå)',
          kredit: 'Översvämningscentret / SYKE vattenprognoser',
          resetZoom: 'Visa hela året',
          resetZoomTitle: 'Visa hela året eller välja genom att måla på grafen'
        }, 
        ice: {
          label: 'istjockhet',
          unit: 'cm',
          yAxisLabelFormat: '{value: .0f}',
          tooltipPointFormatMomentary: '{point.x:%e. %B %Y %H:%M}: {point.y:.0f} cm',
          tooltipPointFormatDay: '{point.x:%e. %B %Y}: {point.y:.0f} cm',
          kredit: 'Översvämningscentret / SYKE vattenprognoser',
          resetZoom: 'Visa hela året',
          resetZoomTitle: 'Visa hela året eller välja genom att måla på grafen'
        },
        sw: {
          label: 'vattennivån',
          unit: 'm',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %B %Y %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %B %Y}: {point.y:.2f} m (NN-nivå)',
          kredit: 'Översvämningscentret / FMI sjöprognoser',
          resetZoom: 'Visa hela året',
          resetZoomTitle: 'Visa hela året eller välja genom att måla på grafen'
        }, 
        mw: {
          label: 'vattennivån',
          unit: 'm',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %B %Y %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %B %Y}: {point.y:.2f} m (NN-nivå)',
          kredit: 'Översvämningscentret / FMI sjöprognoser',
          resetZoom: 'Visa två veckor',
          resetZoomTitle: 'Visa två veckor eller välja genom att måla på grafen'
        }, 
        highestObs: 'Högsta observation under 2000-talet',
        seriesPointInTime: 'Hetkelliset havainnot',
        observed: 'Observerad',
        regulationLimit: 'Regleringsgräns',
        variationLimits: 'Säsongs utbud av vattenstånd under tidigare år',
        average: 'Säsongs genomsnittliga vattenstånd under tidigare år',
        medianForecast: 'Medelprognos',
        forecasted: 'Prognos'                
      };
      Highcharts.setOptions({
        lang: {
          resetZoom: textResources[variable].resetZoom,
          resetZoomTitle: textResources[variable].resetZoomTitle,
          months: ['Januari', 'Februari', 'Mars', 'April', 'Maj', 'Juni', 'Juli', 'Augusti', 'September', 'Oktober', 'November', 'December'],
          shortMonths: ['1', '2', '3', '4', '5', '6',  '7', '8', '9', '10', '11', '12'],
          weekdays: ['Sö', 'Må', 'Ti', 'On', 'To', 'Fr', 'Lö']
        }
      });
    break;    

    case 'en':
      textResources = {
        tw: {
          label: 'pintaveden lämpötila',
          unit: '°C',
          yAxisLabelFormat: '{value: .1f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.1f} °C',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.1f} °C',
          kredit: 'Flood centre / SYKE watershed forecasts',
          resetZoom: 'Show the whole year',
          resetZoomTitle: 'Show the whole year or choose by dragging on the graph'
        },
        gv: {
          label: 'pohjaveden korkeus',
          unit: 'm (N2000-taso)',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.2f} m',
          kredit: 'Flood centre / SYKE watershed forecasts',
          resetZoom: 'Show the whole year',
          resetZoomTitle: 'Show the whole year or choose by dragging on the graph'
        },
        q: {
          label: 'virtaama',
          unit: 'm³/s',
          yAxisLabelFormat: '{value: .1f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.1f} m³/s',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.1f} m³/s',
          kredit: 'Flood centre / SYKE watershed forecasts',
          resetZoom: 'Show the whole year',
          resetZoomTitle: 'Show the whole year or choose by dragging on the graph'
        }, 
        snow: {
          label: 'lumen vesiarvo',
          unit: 'mm',
          yAxisLabelFormat: '{value: .0f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.0f} mm',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.0f} mm',
          kredit: 'Flood centre / SYKE watershed forecasts',
          resetZoom: 'Show the whole year',
          resetZoomTitle: 'Show the whole year or choose by dragging on the graph'
        }, 
        w: {
          label: 'vedenkorkeus',
          unit: 'm',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.2f} m',
          kredit: 'Flood centre / SYKE watershed forecasts',
          resetZoom: 'Show the whole year',
          resetZoomTitle: 'Show the whole year or choose by dragging on the graph'
        }, 
        ice: {
          label: 'jään paksuus',
          unit: 'cm',
          yAxisLabelFormat: '{value: .0f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.0f} cm',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.0f} cm',
          kredit: 'Flood centre / SYKE watershed forecasts',
          resetZoom: 'Show the whole year',
          resetZoomTitle: 'Show the whole year or choose by dragging on the graph'
        },
        sw: {
          label: 'vedenkorkeus',
          unit: 'm',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.2f} m',
          kredit: 'Flood centre / FMI sea forecasts',
          resetZoom: 'Show two weeks',
          resetZoomTitle: 'Show two weeks or choose by dragging on the graph'
        }, 
        mw: {
          label: 'vedenkorkeus',
          unit: 'm',
          yAxisLabelFormat: '{value: .2f}',
          tooltipPointFormatMomentary: '{point.x:%e. %Bta %Y klo %H:%M}: {point.y:.2f} m',
          tooltipPointFormatDay: '{point.x:%e. %Bta %Y}: {point.y:.2f} m',
          kredit: 'Flood centre / FMI sea forecasts',
          resetZoom: 'Show two weeks',
          resetZoomTitle: 'Show two weeks or choose by dragging on the graph'
        }, 
        highestObs: '2000-luvun korkein havainto',
        seriesPointInTime: 'Hetkelliset havainnot',
        observed: 'Havaittu',
        regulationLimit: 'Säännöstelyraja',
        variationLimits: 'Vaihteluväli aiempina vuosina',
        average: 'Keskiarvo aiempina vuosina',
        medianForecast: 'Keskiennuste',
        forecasted: 'Ennustettu',
      };
      Highcharts.setOptions({
        lang: {
          resetZoom: textResources[variable].resetZoom,
          resetZoomTitle: textResources[variable].resetZoomTitle,
          months: ['Tammikuu', 'Helmikuu', 'Maaliskuu', 'Huhtikuu', 'Toukokuu', 'Kesäkuu',  'Heinäkuu', 'Elokuu', 'Syyskuu', 'Lokakuu', 'Marraskuu', 'Joulukuu'],
          shortMonths: ['1', '2', '3', '4', '5', '6',  '7', '8', '9', '10', '11', '12'],
          weekdays: ['Su', 'Ma', 'Ti', 'Ke', 'To', 'Pe', 'La']
        }
      });
    break;                              

  } // switch
    

// ================================================================================================


  let jsonURL = 'https://wwwi2.ymparisto.fi/i2/' + va + '/' + pointId + '/ajo.json';
  $.getJSON(jsonURL, function(data) {

        switch (variable) {
            case 'tw':
                var lyhytennupv = 90;
                var lyhythistpv = 365;
                var pitkaennupv = 90;
                var pitkahistpv = 365;
                kentta_5 = 'tw_5';
                kentta_95 = 'tw_95';
                kentta_ka = 'tw_ka';
                kentta_hav = 'twhav';
                kentta_verhomin = 'tlakeverho_min';
                kentta_verhomax = 'tlakeverho_max';
                kentta_verho50 = 'tlakeverho_50';
                kentta_verho2000 = 'puuttuu_referenssi2000';
                break;
            case 'gv':
                var lyhytennupv = 90;
                var lyhythistpv = 365;
                var pitkaennupv = 90;
                var pitkahistpv = 365;
                kentta_5 = 'gvsim_5';
                kentta_95 = 'gvsim_95';
                kentta_ka = 'gvsim_ka';
                kentta_hav = 'gvobs';
                kentta_verhomin = 'gvsimverho_min';
                kentta_verhomax = 'gvsimverho_max';
                kentta_verho50 = 'gvsimverho_med';
                kentta_verho2000 = 'gvsimverho_hgv2000';
                break;
            case 'q':
                var lyhytennupv = 8;
                var lyhythistpv = 7;
                var pitkaennupv = 90;
                var pitkahistpv = 365;
                kentta_5 = 'qinlask_5';
                kentta_95 = 'qinlask_95';
                kentta_ka = 'qinlask_ka';
                kentta_hav = 'qhav';
                kentta_verhomin = 'qinverho_min';
                kentta_verhomax = 'qinverho_max';
                kentta_verho50 = 'qinverho_50';
                kentta_verho2000 = 'puuttuu_referenssi2000';
                break;
            case 'snow':
                var lyhytennupv = 90;
                var lyhythistpv = 365;
                var pitkaennupv = 90;
                var pitkahistpv = 365;
                kentta_5 = 'lumilinja_5';
                kentta_95 = 'lumilinja_95';
                kentta_ka = 'lumilinja_ka';
                kentta_hav = 'snowobs';
                kentta_verhomin = 'snowverho_min';
                kentta_verhomax = 'snowverho_max';
                kentta_verho50 = 'snowverho_50';
                kentta_verho2000 = 'puuttuu_referenssi2000';
                break;
            case 'w':
                //if (pointId == 'l041121001y') {
                if (onkoisojarvi(pointId)) {
                  var lyhytennupv = 180;
                  var lyhythistpv = 365;
                  var pitkaennupv = 180;
                  var pitkahistpv = 365;
                } else {
                  var lyhytennupv = 30;
                  var lyhythistpv = 30;
                  var pitkaennupv = 30;
                  var pitkahistpv = 365;
                }
                if (lq == 'q') {
                  var lyhytennupv = 8;
                  var lyhythistpv = 7;
                  var pitkaennupv = 90;
                  var pitkahistpv = 365;
                }
                kentta_5 = 'wlask_5';
                kentta_95 = 'wlask_95';
                kentta_ka = 'wlask_ka';
                kentta_hav = 'whav';
                kentta_verhomin = 'w1verho_min';
                kentta_verhomax = 'w1verho_max';
                kentta_verho50 = 'w1verho_50';
                kentta_verho2000 = 'w1verho_hw2000';
                break;
            case 'ice':
                var lyhytennupv = 90;
                var lyhythistpv = 365;
                var pitkaennupv = 90;
                var pitkahistpv = 365;
                kentta_5 = 'ice_5';
                kentta_95 = 'ice_95';
                kentta_ka = 'ice_ka';
                kentta_hav = 'ice_hav';
                kentta_verhomin = 'puuttuu_referenssi_min';
                kentta_verhomax = 'puuttuu_referenssi_max';
                kentta_verho50 = 'puuttuu_referenssi_med';
                kentta_verho2000 = 'puuttuu_referenssi2000';
                break; 
            case 'sw':
                var lyhytennupv = 3;
                var lyhythistpv = 2;
                var pitkaennupv = 3;
                var pitkahistpv = 14;
                kentta_5 = 'puuttuu_wlask_5';
                kentta_95 = 'puuttuu_wlask_95';
                kentta_ka = 'puuttuu_wlask_ka';
                kentta_hav = 'puuttuu_whav';
                kentta_verhomin = 'w1verho_min';
                kentta_verhomax = 'w1verho_max';
                kentta_verho50 = 'w1verho_mean';
                kentta_verho2000 = 'puuttuu_referenssi2000';
                break;               
        }

        // var maxyarvo = -90000;
        var maxyarvo = null;
        
        var ennustepvm_www=data.ennustepvm_www;
        var tmp=ennustepvm_www.split("-");
        var epvm = new Date(parseInt(tmp[0]),parseInt(tmp[1])-1,parseInt(tmp[2]));//, 0, 0, 0));
        var msec = Date.parse(epvm);
        var ennupvm = new Date(msec);
        var ennuv = ennupvm.getFullYear();
        var ennuk = ennupvm.getMonth();
        var ennup = ennupvm.getDate();
        var ennusteen_alku = Date.UTC(ennuv, ennuk, ennup);
        if (variable == 'sw') {
            var ennusteen_alku = minuuttihavaintojenloppu; 
        }

        var kuvanalkupvm = new Date(msec);
        kuvanalkupvm.setDate(kuvanalkupvm.getDate()-lyhythistpv);
        var kuvanalkuv = kuvanalkupvm.getFullYear();
        var kuvanalkuk = kuvanalkupvm.getMonth();
        var kuvanalkup = kuvanalkupvm.getDate();

        var kuvanloppupvm = new Date(msec);
        kuvanloppupvm.setDate(kuvanloppupvm.getDate()+lyhytennupv);
        var kuvanloppuv = kuvanloppupvm.getFullYear();
        var kuvanloppuk = kuvanloppupvm.getMonth();
        var kuvanloppup = kuvanloppupvm.getDate();

        var pitkakuvanloppupvm = new Date(msec);
        pitkakuvanloppupvm.setDate(pitkakuvanloppupvm.getDate()+pitkaennupv); 
        var pitkakuvanloppuv = pitkakuvanloppupvm.getFullYear();
        var pitkakuvanloppuk = pitkakuvanloppupvm.getMonth();
        var pitkakuvanloppup = pitkakuvanloppupvm.getDate();
        loppuvvvvkkpp = pitkakuvanloppuv*10000 + pitkakuvanloppuk*100 + pitkakuvanloppup;

        var pitkakuvanalkupvm = new Date(msec);
        pitkakuvanalkupvm.setDate(pitkakuvanalkupvm.getDate()-pitkahistpv); 
        var pitkakuvanalkuv = pitkakuvanalkupvm.getFullYear();
        var pitkakuvanalkuk = pitkakuvanalkupvm.getMonth();
        var pitkakuvanalkup = pitkakuvanalkupvm.getDate();
        alkuvvvvkkpp = pitkakuvanalkuv*10000 + pitkakuvanalkuk*100 + pitkakuvanalkup;

        var jakaumaon=data[kentta_5];
        if (typeof jakaumaon !== 'undefined') {
            var jakaumapvm_www=data[kentta_5].ensim_pvm;
            var tmp=jakaumapvm_www.split("-");
            var jpvm = new Date(parseInt(tmp[0]),parseInt(tmp[1])-1,parseInt(tmp[2]));
            var msec = Date.parse(jpvm);
            var jakaumapvm = new Date(msec);
            //
            var xx_5_ensim_pvm=data[kentta_5].ensim_pvm;
            var xx_5_lst=data[kentta_5].lst;
            var xx_95_ensim_pvm=data[kentta_95].ensim_pvm;
            var xx_95_lst=data[kentta_95].lst;
            var ennustejakauma = aikaSarja("enn",jakaumapvm,xx_5_ensim_pvm,xx_5_lst,xx_95_lst);
        } else {
            var ennustejakauma;
        }

        var simjaennuon=data[kentta_ka];
        if (typeof simjaennuon !== 'undefined') {
            var xx_ka_ensim_pvm=data[kentta_ka].ensim_pvm;
            var xx_ka_lst=data[kentta_ka].lst;
            var simuloitu = aikaSarja("hav",jakaumapvm,xx_ka_ensim_pvm,xx_ka_lst); 
            var xx_ka_ensim_pvm=data[kentta_ka].ensim_pvm;
            var xx_ka_lst=data[kentta_ka].lst;
            var ennustettu = aikaSarja("enn",jakaumapvm,xx_ka_ensim_pvm,xx_ka_lst);
            var simjaennuLegendiin = true;
        } else {
            var simuloitu;
            var ennustettu;
            var simjaennuLegendiin = false;
        }
        
        var havon=data[kentta_hav];
        if (typeof havon !== 'undefined') {
            var vrkhav_ensim_pvm=data[kentta_hav].ensim_pvm;
            var vrkhav_lst=data[kentta_hav].lst;
            var vrkhav = aikaSarja("hav",jakaumapvm,vrkhav_ensim_pvm,vrkhav_lst); 
        } else {
            var vrkhav;
        }

        var refon=data[kentta_verhomin];
        if (typeof refon !== 'undefined') {
            var refmin_lst=data[kentta_verhomin];
            var refmax_lst=data[kentta_verhomax];
            var refjakauma = vuosiSarja(refmin_lst,refmax_lst);
            var refmed_lst=data[kentta_verho50];
            var refmediaani = vuosiSarja(refmed_lst);
            // 2000-luvun korkein
            var h2000on=data[kentta_verho2000];
            if (typeof h2000on !== 'undefined') {
              var h2000=data[kentta_verho2000];
              if (maxyarvo < h2000) {maxyarvo=h2000};
            } else {
              var h2000="90000";
            }
            var refLegendiin = true;      
        } else {
            var refjakauma;
            var refmediaani;
            var h2000="90000";
            var refLegendiin = false;
        }

        var srajaon=data.sraja;
        if (typeof srajaon !== 'undefined' && variable == "w") {
            var sraja_lst=data.sraja;
            var sraja = vuosiSarja(sraja_lst);
            var srajaLegendiin = true;
        } else {
            var sraja;
            var srajaLegendiin = false;
        }

        var w1nollaon=data.w1nolla;
        if (typeof w1nollaon !== 'undefined' && variable == "w") {
            var wtasotxt='(' + data.w1nolla + '-taso)'
        } else {
            var wtasotxt="";
        }

        var vahinkoraja1raja="90000";
        var vahinkoraja1luokka="";
        var vahinkoraja1txt="";
        var vahinkoraja1on=data.vahinkoraja_puuttuu;
        if (variable == "w") {
          vahinkoraja1on=data.w1vahinkoraja;
          if (typeof vahinkoraja1on !== 'undefined') {
            vahinkoraja1raja=data.w1vahinkoraja[0];
            vahinkoraja1luokka=data.w1vahinkoraja[1];
            vahinkoraja1txt=data.w1vahinkoraja[2];
          }
        }
        if (variable == "q") {
          vahinkoraja1on=data.q1vahinkoraja;
          if (typeof vahinkoraja1on !== 'undefined') {
            vahinkoraja1raja=data.q1vahinkoraja[0];
            vahinkoraja1luokka=data.q1vahinkoraja[1];
            vahinkoraja1txt=data.q1vahinkoraja[2];
          }
        }
        if (typeof vahinkoraja1on !== 'undefined') {
          switch (vahinkoraja1luokka) {
            case 'musta':
              var vahinkoraja1vari="#000000";
              var vahinkoraja1tyyli="dash";
              var vahinkoraja1leveys=1;
              break;
            case 'keltainen':
              var vahinkoraja1vari="#FEEC00";
              var vahinkoraja1tyyli="dash";
              var vahinkoraja1leveys=2;
              break;
            case 'oranssi':
              var vahinkoraja1vari="#FF7A09";
              var vahinkoraja1tyyli="shortdash";
              var vahinkoraja1leveys=2;
              break;
            }
          if (maxyarvo < vahinkoraja1raja) {maxyarvo=vahinkoraja1raja};
        }

        var vahinkoraja2raja="90000";
        var vahinkoraja2luokka="";
        var vahinkoraja2txt="";
        var vahinkoraja2on=data.vahinkoraja_puuttuu;
        if (variable == "w") {
          vahinkoraja2on=data.w2vahinkoraja;
          if (typeof vahinkoraja2on !== 'undefined') {
            vahinkoraja2raja=data.w2vahinkoraja[0];
            vahinkoraja2luokka=data.w2vahinkoraja[1];
            vahinkoraja2txt=data.w2vahinkoraja[2];
          }
        }
        if (variable == "q") {
          vahinkoraja2on=data.q2vahinkoraja;
          if (typeof vahinkoraja2on !== 'undefined') {
            vahinkoraja2raja=data.q2vahinkoraja[0];
            vahinkoraja2luokka=data.q2vahinkoraja[1];
            vahinkoraja2txt=data.q2vahinkoraja[2];
          }
        }
        if (typeof vahinkoraja2on !== 'undefined') {
          switch (vahinkoraja2luokka) {
            case 'musta':
              var vahinkoraja2vari="#000000";
              var vahinkoraja2tyyli="dash";
              var vahinkoraja2leveys=1;
              break;
            case 'keltainen':
              var vahinkoraja2vari="#FEEC00";
              var vahinkoraja2tyyli="dash";
              var vahinkoraja2leveys=2;
              break;
            case 'oranssi':
              var vahinkoraja2vari="#FF7A09";
              var vahinkoraja2tyyli="shortdash";
              var vahinkoraja2leveys=2;
              break;
            }
          if (maxyarvo < vahinkoraja2raja) {maxyarvo=vahinkoraja2raja};
        }

        var vahinkoraja3raja="90000";
        var vahinkoraja3luokka="";
        var vahinkoraja3txt="";
        var vahinkoraja3on=data.vahinkoraja_puuttuu;
        if (variable == "w") {
          vahinkoraja3on=data.w3vahinkoraja;
          if (typeof vahinkoraja3on !== 'undefined') {
            vahinkoraja3raja=data.w3vahinkoraja[0];
            vahinkoraja3luokka=data.w3vahinkoraja[1];
            vahinkoraja3txt=data.w3vahinkoraja[2];
          }
        }
        if (variable == "q") {
          vahinkoraja3on=data.q3vahinkoraja;
          if (typeof vahinkoraja3on !== 'undefined') {
            vahinkoraja3raja=data.q3vahinkoraja[0];
            vahinkoraja3luokka=data.q3vahinkoraja[1];
            vahinkoraja3txt=data.q3vahinkoraja[2];
          }
        }
        if (typeof vahinkoraja3on !== 'undefined') {
          switch (vahinkoraja3luokka) {
            case 'musta':
              var vahinkoraja3vari="#000000";
              var vahinkoraja3tyyli="dash";
              var vahinkoraja3leveys=1;
              break;
            case 'keltainen':
              var vahinkoraja3vari="#FEEC00";
              var vahinkoraja3tyyli="dash";
              var vahinkoraja3leveys=2;
              break;
            case 'oranssi':
              var vahinkoraja3vari="#FF7A09";
              var vahinkoraja3tyyli="shortdash";
              var vahinkoraja3leveys=2;
              break;
            }
          if (maxyarvo < vahinkoraja3raja) {maxyarvo=vahinkoraja3raja};
        }

        var tulvakartta20on=data.tulvakarttaraja20;
        if (typeof tulvakartta20on !== 'undefined' && variable == "w") {
            var tulvakarttaraja20=data.tulvakarttaraja20;
            if (maxyarvo < tulvakarttaraja20) {maxyarvo=tulvakarttaraja20};
        } else {
            var tulvakarttaraja20;
        }

        var tulvakartta100on=data.tulvakarttaraja100;
        if (typeof tulvakartta100on !== 'undefined' && variable == "w") {
            var tulvakarttaraja100=data.tulvakarttaraja100;
            if (maxyarvo < tulvakarttaraja100) {maxyarvo=tulvakarttaraja100};
        } else {
            var tulvakarttaraja100;
        }


// ================================================================================================


        $(targetElementID).highcharts({

            title: {
                text: pointname + ' ' + textResources[variable].label
            },

            subtitle: {
              align: 'left',
              text: textResources[variable].unit + ' ' + wtasotxt
            },


            yAxis: {
              title: {text: null},
              allowDecimals: true,
              labels: {format: textResources[variable].yAxisLabelFormat},
              offset: -13,
              startOnTick: false,
              endOnTick: false,
              id: 'minun_y_akseli',
              // max: maxyarvo,
              plotLines: [

                {
                  value: h2000,
                  color: '#666666',
                  dashStyle: 'dash',
                  width: 1,
                  zIndex: 7,
                  label: {
                    text: textResources.highestObs,
                    align: 'left'
                  },
                },

                {
                  value : vahinkoraja1raja,
                  color : vahinkoraja1vari,
                  dashStyle : vahinkoraja1tyyli,
                  width : vahinkoraja1leveys,
                  zIndex : 8,
                  label : {text : vahinkoraja1txt}
                },

                 {
                  value : vahinkoraja2raja,
                  color : vahinkoraja2vari,
                  dashStyle : vahinkoraja2tyyli,
                  width : vahinkoraja2leveys,
                  zIndex : 8,
                  label : {text : vahinkoraja2txt}
                },

                {
                  value : vahinkoraja3raja,
                  color : vahinkoraja3vari,
                  dashStyle : vahinkoraja3tyyli,
                  width : vahinkoraja3leveys,
                  zIndex : 8,
                  label : {text : vahinkoraja3txt}
                },

                {
                  value : tulvakarttaraja20,
                  color : '#8B42A8B3',
                  dashStyle : 'solid',
                  width : 6,
                  zIndex : 7,
                  label : {
                    text : 'Tulvakartta: Melko yleinen tulva',
                    align: 'right',
                    x: -10
                  }
               },

               {
                 value : tulvakarttaraja100,
                 color : '#E284EAB3',
                 dashStyle : 'solid',
                 width : 6,
                 zIndex : 7,
                 label : {
                   text : 'Tulvakartta: Harvinainen tulva',
                   align: 'right',
                   x: -10
                 }
               },

                    
            ]
            },

            chart: {
              zoomType: 'x',
              resetZoomButton: {
                position: {
                  y: 30,
                  //align: 'right',
                  //verticalAlign: 'top'
                },
                relativeTo: 'chart'
              },
            },

            legend: {
              itemStyle: {
                fontWeight: 'normal',
                width: window.innerWidth - 25 +'px'
              },
            },


            xAxis: {
              type: 'datetime',
              id: 'minun_x_akseli',
              // tickPixelInterval: 40,
              dateTimeLabelFormats: {
                hour: '<i>%H:00</i>',
                day: '%A<br/>%e.%b',
                week: '%e.%b',
                month: '%Y<br/>%B',
                year: '%Y'
              },
              plotLines: [{
                value: ennusteen_alku,
                color: '#EEEEEE',
                dashStyle: 'solid',
                width: 2,
                zIndex: 7
              }]
            },

            credits: {
              enabled: true,
              text: textResources[variable].kredit,
              position: {
                align: 'center'
              },
              href: '',
              style: {
                cursor: 'arrow'
              }
            },

            tooltip: {
              hideDelay: 100,
              shape: "square",
              positioner: function() {
                return { x: 70, y: 270 };
              }
            },

            plotOptions: {
              series: {
                stickyTracking: false
              }
            },


            series: [

              {
                name: textResources.seriesPointInTime,
                data: minuuttihavainnot,
                showInLegend: false,
                color: '#0000AA',
                lineWidth: 0,
                marker: {
                  enabled: true,
                  symbol: 'circle',
                  radius: 2,
                  fillColor: 'white',
                  lineWidth: 1,
                  lineColor: '#0000AA'
                },
                tooltip: {
                  headerFormat: '<b>' + textResources.observed + ' ' +  textResources[variable].label + '</b><br>',  
                  pointFormat: textResources[variable].tooltipPointFormatMomentary
                },
                legendIndex: 1,
                zIndex: 6
              },

              {
                name: textResources.seriesPointInTime,
                data: minuuttiennuste,
                showInLegend: false,
                color: '#0000AA',
                lineWidth: 0,
                marker: {
                  enabled: true,
                  symbol: 'circle',
                  radius: 2,
                  fillColor: 'white',
                  lineWidth: 1,
                  lineColor: '#AA0000'
                },
                tooltip: {
                  headerFormat: '<b>' + textResources.forecasted + ' ' +  textResources[variable].label + '</b><br>',  
                  pointFormat: textResources[variable].tooltipPointFormatMomentary
                },
                legendIndex: 1,
                zIndex: 7
              },

              {
                name: 'Vrkhavainto',
                data: vrkhav,
                visible: true,
                showInLegend: false,
                color: '#0000AA',
                lineWidth: 0,
                marker: {
                  enabled: true,
                  symbol: 'circle',
                  radius: 3,
                  fillColor: 'white',
                  lineWidth: 1,
                  lineColor: '#0000AA'
                },
                tooltip: {
                  headerFormat: '<b>' + textResources.observed + ' ' +  textResources[variable].label + '</b><br>',
                  pointFormat: textResources[variable].tooltipPointFormatDay
                },
                zIndex: 8
              },

              {
                name: textResources.regulationLimit,
                data: sraja,
                showInLegend: srajaLegendiin,
                color: '#333333',
                marker: {
                  enabled: false
                },
                enableMouseTracking: false,
                lineWidth: 2,
                dashStyle: 'DashDot',
                legendIndex: 9,
                zIndex: 6
              },

              {
                name:  textResources[variable].label.substr(0,1).toUpperCase() + textResources[variable].label.substr(1),
                data: simuloitu,
                showInLegend: simjaennuLegendiin,
                marker: {
                  enabled: false,
                  states: {
                    hover: {enabled: false}
                  }
                },
                color: '#0066EE',
                enableMouseTracking: false,
                legendIndex: 1,
                zIndex: 7
              },

              {
                name: textResources.variationLimits,
                data: refjakauma,
                showInLegend: refLegendiin,
                type: 'arearange',
                color: '#CCCCCC',
                enableMouseTracking: false,
                legendIndex: 5,
                zIndex: 1
              },

              {
                name: textResources.average,
                data: refmediaani,
                showInLegend: refLegendiin,
                color: '#999999',
                marker: {
                  enabled: false
                },
                enableMouseTracking: false,
                legendIndex: 6,
                zIndex: 2
              },

              {
                name: textResources.medianForecast,
                data: ennustettu,
                showInLegend: simjaennuLegendiin,
                visible: false,
                marker: {
                  enabled: false,
                  states: {
                    hover: {enabled: false}
                  }
                },
                color: '#0066EE',
                enableMouseTracking: false,
                legendIndex: 3,
                zIndex: 7
              },

              {
                name: textResources.forecasted + ' ' +  textResources[variable].label,
                data: ennustejakauma,
                showInLegend: simjaennuLegendiin,
                type: 'arearange',
                color: '#0088FF',
                enableMouseTracking: false,
                legendIndex: 2,
                zIndex: 4
              },

              {
                name: 'Max y plotlineista',
                data: [[Date.UTC(pitkakuvanalkuv, pitkakuvanalkuk, pitkakuvanalkup), maxyarvo],[Date.UTC(pitkakuvanloppuv, pitkakuvanloppuk, pitkakuvanloppup), maxyarvo]],
                showInLegend: false, 
                lineWidth: 0,
                marker: {enabled: false},
                enableMouseTracking: false,
                zIndex: 1
              }


            ]

        }); // graaficontaineri


        var chart = $(targetElementID).highcharts();
        var yAxis = chart.get('minun_y_akseli');
        var xAxis = chart.get('minun_x_akseli');
        xAxis.setExtremes(Date.UTC(kuvanalkuv,kuvanalkuk,kuvanalkup), Date.UTC(kuvanloppuv,kuvanloppuk,kuvanloppup));
        var extremes = yAxis.getExtremes();
        switch (variable) {
            case 'tw':
                yAxis.setExtremes(Math.min(0,extremes.min),Math.max(5,maxyarvo,extremes.max));
                break;
            case 'gv':
                yAxis.setExtremes(extremes.min,Math.max(maxyarvo,extremes.max));
                break;
            case 'q':
                yAxis.setExtremes(Math.min(0,extremes.min),Math.max(0,maxyarvo,extremes.max));
                break;
            case 'snow':
                yAxis.setExtremes(0,Math.max(20,maxyarvo,extremes.max));
                break;
            case 'w':
                yAxis.setExtremes(extremes.min,Math.max(0,maxyarvo,extremes.max));
                break;
            case 'ice':
                yAxis.setExtremes(Math.min(-5,extremes.min),0);
                break;        
            case 'sw':
                yAxis.setExtremes(extremes.min,extremes.max);
                break;        
            default:
                yAxis.setExtremes(Math.min(0,extremes.min),Math.max(5,maxyarvo,extremes.max));
                break;
        }
        if (lyhythistpv != pitkahistpv) {
          chart.showResetZoom();
        }
    });    
}


// ================================================================================================


function aikaSarja(HavEnn,jakaumapvm,alku_pvm,arvot1,arvot2)
{
  // HavEnn=hav/enn/koko (havaintojakso, ennustejakso vai koko kuvajakso)
  var aikasarja=[];
  var tmp=alku_pvm.split("-");
  var alku_pvm = new Date(parseInt(tmp[0]),parseInt(tmp[1])-1,parseInt(tmp[2]));
  for (var i = 0; i < arvot1.length; i++) {
    var newdate = new Date(alku_pvm);
    newdate.setDate(newdate.getDate() + i);
    var uusi=new Date(parseInt(newdate.getFullYear()),parseInt(newdate.getMonth()),parseInt(newdate.getDate()));
    var msec = Date.parse(uusi);
    var alkupvm = new Date(msec);
    var alkuv = alkupvm.getFullYear();
    var alkuk = alkupvm.getMonth();
    var alkup = alkupvm.getDate();
    if (msec <= jakaumapvm) {
      havjakso=true;
    } else {
      havjakso=false;
    }
    if (msec >= jakaumapvm) {
      ennjakso=true;
    } else {
      ennjakso=false;
    }
    var vvvvkkpp = alkuv*10000 + alkuk*100 + alkup;
    if ((vvvvkkpp >= alkuvvvvkkpp) && (vvvvkkpp <= loppuvvvvkkpp)) {
      if ((HavEnn == "koko") || (HavEnn == "hav" && havjakso == true) || (HavEnn == "enn" && ennjakso == true)) {
        if (arvot2 != undefined) {
          aikasarja.push([Date.UTC(alkuv, alkuk, alkup),arvot1[i],arvot2[i]]);
        } else {
          aikasarja.push([Date.UTC(alkuv, alkuk, alkup),arvot1[i]]);
        }
      }
    }
  }
  return aikasarja
}


function vuosiSarja(arvot1,arvot2)
{
  var aikasarja=[];
  var d = new Date();
  var y = d.getFullYear()-1;
  var alku_pvm = new Date(y, 00, 01);
  for (var y = 0; y < 3; y++) {
    for (var i = 0; i < arvot1.length; i++) {
      var newdate = new Date(alku_pvm);
      newdate.setDate(newdate.getDate() + i);
      var uusi=new Date(parseInt(newdate.getFullYear()+y),parseInt(newdate.getMonth()),parseInt(newdate.getDate()));
      var msec = Date.parse(uusi);
      var alkupvm = new Date(msec);
      var alkuv = alkupvm.getFullYear();
      var alkuk = alkupvm.getMonth();
      var alkup = alkupvm.getDate();
      var vvvvkkpp = alkuv*10000 + alkuk*100 + alkup;
      if ((vvvvkkpp >= alkuvvvvkkpp) && (vvvvkkpp <= loppuvvvvkkpp)) {
        if (arvot2 != undefined) {
          if (arvot1[i] > 89999) {arvot1[i] = null} // Puuttuvat tw:t on referensseissä
          if (arvot2[i] > 89999) {arvot2[i] = null} // merkattu 90000.0000
          aikasarja.push([Date.UTC(alkuv, alkuk, alkup),arvot1[i],arvot2[i]]);
        } else {
          if (arvot1[i] > 89999) {arvot1[i] = null}
          aikasarja.push([Date.UTC(alkuv, alkuk, alkup),arvot1[i]]);
        }
      }
    }
  }
  return aikasarja
}


function onkoisojarvi(piste)
{
  var isojarvi=false;
  var p=piste.slice(0,10);
  if (p == "l030311001") {isojarvi = true}; // #Simpelejärvi
  if (p == "l041121001") {isojarvi = true}; // #Saimaa Lauritsala
  if (p == "l041211001") {isojarvi = true}; // #Pihlajavesi Savonlinna ala
  if (p == "l041411001") {isojarvi = true}; // #Kuolimo
  if (p == "l041811001") {isojarvi = true}; //
  if (p == "l042111001") {isojarvi = true}; // #Haukivesi
  if (p == "l042211001") {isojarvi = true}; // #Enonvesi
  if (p == "l042631001") {isojarvi = true}; // #Sorsavesi
  if (p == "l042711001") {isojarvi = true}; // #Unnukka, Taipale ylä
  if (p == "l042721001") {isojarvi = true}; // #Kallavesi Itkonniemi
  if (p == "l042731001") {isojarvi = true}; // #Suvasvesi
  if (p == "l042741014") {isojarvi = true}; // #Kermajärvi
  if (p == "l042811001") {isojarvi = true}; // #Kallavesi
  if (p == "l043111001") {isojarvi = true}; // #Orivesi
  if (p == "l043211001") {isojarvi = true}; // #Orivesi-Pyhäselkä
  if (p == "l043521001") {isojarvi = true}; // #Viinijärvi
  if (p == "l043911001") {isojarvi = true}; // #Pyhäjärvi
  if (p == "l044111001") {isojarvi = true}; // #Pielinen Ahveninen
  if (p == "l045111001") {isojarvi = true}; // #Onkivesi
  if (p == "l046111001") {isojarvi = true}; //
  if (p == "l046211001") {isojarvi = true}; // #Vuotjärvi
  if (p == "l046311001") {isojarvi = true}; // #Syväri
  if (p == "l047111004") {isojarvi = true}; // #Juojärvi
  if (p == "l047211001") {isojarvi = true}; //
  if (p == "l048211001") {isojarvi = true}; // #Höytiäinen
  if (p == "l049221001") {isojarvi = true}; // #Nuorajärvi
  if (p == "l049411001") {isojarvi = true}; // #Koitere Surinkivi
  if (p == "l141211004") {isojarvi = true}; // #Pyhäjärvi
  if (p == "l141311001") {isojarvi = true}; // #Konnivesi
  if (p == "l141411001") {isojarvi = true}; // #Ruotsalainen Heinola
  if (p == "l141921001") {isojarvi = true}; // #Ylä-Kivijärvi
  if (p == "l142111001") {isojarvi = true}; // #Päijänne Kalkkinen
  if (p == "l142211001") {isojarvi = true}; // #Päijänne Tehinselkä
  if (p == "l142311001") {isojarvi = true}; // #Päijänne pohjoinen
  if (p == "l142411001") {isojarvi = true}; // #Vesijärvi
  if (p == "l143111001") {isojarvi = true}; // #Leppävesi
  if (p == "l143521001") {isojarvi = true}; // #Kynsivesi
  if (p == "l143911001") {isojarvi = true}; // #Lievestuoreenjärvi
  if (p == "l144111001") {isojarvi = true}; // #Ala-Keitele
  if (p == "l144211001") {isojarvi = true}; // #Keski-Keitele
  if (p == "l144311001") {isojarvi = true}; // #Ylä-Keitele
  if (p == "l144411001") {isojarvi = true}; // #Vuosjärvi
  if (p == "l144431001") {isojarvi = true}; // #Kivijärvi
  if (p == "l144721001") {isojarvi = true}; // #Kolima
  if (p == "l144811001") {isojarvi = true}; // #Alvajärvi
  if (p == "l146811001") {isojarvi = true}; // #Pyhäjärvi
  if (p == "l147111001") {isojarvi = true}; // #Konnevesi
  if (p == "l147211001") {isojarvi = true}; // #Niinivesi
  if (p == "l147221001") {isojarvi = true}; // #Iisvesi
  if (p == "l147311001") {isojarvi = true}; // #Nilakka
  if (p == "l147411001") {isojarvi = true}; // #Pielavesi
  if (p == "l147821001") {isojarvi = true}; // #Suontienselkä
  if (p == "l148211001") {isojarvi = true}; // #Jääsjärvi
  if (p == "l148311001") {isojarvi = true}; // #Rautavesi
  if (p == "l148411001") {isojarvi = true}; // #Suontee
  if (p == "l148511001") {isojarvi = true}; // #Suontee
  if (p == "l149121001") {isojarvi = true}; // #Vuohijärvi
  if (p == "l149221001") {isojarvi = true}; // #Puula / Ryökäsvesi-Liekune
  if (p == "l149231001") {isojarvi = true}; // #Puula
  if (p == "l149321001") {isojarvi = true}; // #Kyyvesi
  if (p == "l230211001") {isojarvi = true}; // #Lohjanjärvi
  if (p == "l340311001") {isojarvi = true}; // #Pyhäjärvi
  if (p == "l351321001") {isojarvi = true}; // #Kulovesi
  if (p == "l352111001") {isojarvi = true}; // #Pyhäjärvi
  if (p == "l352221001") {isojarvi = true}; // #Vanaja / Toijala
  if (p == "l352311001") {isojarvi = true}; // #Vanaja-Vanajanselkä
  if (p == "l353111001") {isojarvi = true}; // #Näsijärvi
  if (p == "l353121001") {isojarvi = true}; // #Näsijärvi
  if (p == "l353311001") {isojarvi = true}; // #Ruovesi
  if (p == "l354111001") {isojarvi = true}; // #Tarjanne
  if (p == "l354121001") {isojarvi = true}; // #Tarjanne, Visuvesi
  if (p == "l354331001") {isojarvi = true}; // #Ähtärinjärvi
  if (p == "l355211001") {isojarvi = true}; // #Kyrösjärvi
  if (p == "l356211001") {isojarvi = true}; // #Keurusselkä
  if (p == "l357111001") {isojarvi = true}; // #Mallasvesi
  if (p == "l357131001") {isojarvi = true}; // #Roine
  if (p == "l357211001") {isojarvi = true}; // #Längelmävesi
  if (p == "l357221001") {isojarvi = true}; // #Koljonselkä
  if (p == "l357311001") {isojarvi = true}; // #Vesijärvi
  if (p == "l357811002") {isojarvi = true}; // #Kukkia
  if (p == "l360141001") {isojarvi = true}; // #Isojärvi
  if (p == "l470311001") {isojarvi = true}; // #Lappajärvi
  if (p == "l510411001") {isojarvi = true}; // #Lestijärvi
  if (p == "l540511001") {isojarvi = true}; // #Pyhäjärvi
  if (p == "l593111001") {isojarvi = true}; // #Oulujärvi Vaala
  if (p == "l593211001") {isojarvi = true}; // #Oulujärvi Vuottolahti
  if (p == "l593311001") {isojarvi = true}; // #Oulujärvi Melalahti
  if (p == "l595111001") {isojarvi = true}; // #Kiantajärvi
  if (p == "l596211001") {isojarvi = true}; // #Vuokkijärvi
  if (p == "l598111001") {isojarvi = true}; // #Rehja-Nuasjärvi
  if (p == "l599111001") {isojarvi = true}; // #Ontojärvi
  if (p == "l599121001") {isojarvi = true}; // #Lammasjärvi
  if (p == "l599211001") {isojarvi = true}; // #Lentua
  if (p == "l616221001") {isojarvi = true}; // #Kostonjärvi
  if (p == "l640511001") {isojarvi = true}; // #Simojärvi
  if (p == "l640521001") {isojarvi = true};
  if (p == "l653111001") {isojarvi = true}; // #Kemijärvi ala
  if (p == "l653921001") {isojarvi = true}; // #Ala-Suolijärvi - Oivanjärvi
  if (p == "l658312001") {isojarvi = true}; // #Porttipahdan tekojärvi
  if (p == "l659312001") {isojarvi = true}; // #Lokan tekojärvi
  if (p == "l676401001") {isojarvi = true}; // #Kilpisjärvi - Alajärvi
  if (p == "l679311001") {isojarvi = true}; // #Miekojärvi
  if (p == "l679611001") {isojarvi = true}; // #Vietonen
  if (p == "l690311001") {isojarvi = true}; // #Iijärvi
  if (p == "l711111001") {isojarvi = true}; // #Inarijärvi Inari
  if (p == "l711711001") {isojarvi = true};
  if (p == "l712411001") {isojarvi = true}; // #Mutusjärvi
  if (p == "l730241001") {isojarvi = true}; // #Ala-Kitka
  if (p == "l730251001") {isojarvi = true}; // #Yli-Kitka
  if (p == "l740211001") {isojarvi = true}; // #Muojärvi-Kirpistö
  if (p == "l740311001") {isojarvi = true}; // #Kuusamojärvi
  if (p == "l991101001") {isojarvi = true};
  return isojarvi;
}