/* JSON-tiedoston url, html-elementin id jossa pikanäkymä näytetään */
// getPikanakymaJSON("//wwwi2.ymparisto.fi/i2/pikanakyma.json", 'kohdetiedot');




/* TÄMÄ HAKEE KÄYNNISTÄÄ JSON-TIEDOSTON HAKEMISEN, JA JOS DATAA LÖYTYY ALOITTAA PIKANÄKYMÄN GENEROINNIN */
function getPikanakymaJSON(JSONurl, suure, placename, pikanakymaTargetID, includeGraphLink=false) {
	//testaus palvelimella ilman asetettua http- tai https-protokollaa
	//var JSONdataObj = JSON.parse( getDataObj("//wwwi2.ymparisto.fi/i2/pikanakyma.json"));
	// testaus lokaalisti
	//var JSONdataObj = JSON.parse( getDataObj("http://wwwi2.ymparisto.fi/i2/pikanakyma.json"));
	//testaus demopalvelimen json-tiedostoa modaamalla
	//var JSONdataObj = JSON.parse( getDataObj("//www.dadvertising.fi/vesifi_demo/pikanakyma/pikanakyma.json"));

	// var JSONdataObj = JSON.parse( getDataObj(JSONurl));
	let JSONrequestPromise = getDataObj(JSONurl);
	JSONrequestPromise.then(function (result) {
		if (result != null) {
			var JSONdataObj = JSON.parse(result);
			if (JSONdataObj.aikaleima != '' && JSONdataObj.aikaleima != undefined && JSONdataObj.taso != '' && JSONdataObj.taso != undefined) {
				teePikanakyma(suure, placename, JSONdataObj, pikanakymaTargetID, JSONurl, includeGraphLink);
				return;
			}
		}
	}).catch(function (exception) {
		console.error(exception);
	});
}
/* 
TÄMÄ KOODI TEKEE SEN VARSINAISEN GRAAFIN,            border-radius: 5px;
    border: 1px solid #000000;
parametrina teema (JSON-tiedostosta), JSONdataObj,  html-elementin id jossa pikanäkymä näytetään    <button id="btnShare" title="Jaa linkki" class="zoomButtonLast hiddenToolsMiddleBorders"><img src="./img/SYKE_Map_Share.svg" class="buttonIcon"></button>
*/
function teePikanakyma(suure, paikannimi, dataObj, targetID, JSONurl, includeGraphLink=false) {
	var pnTemplate = '<div id="pikanakyma" class="pikanakyma" style="display:none;">' + 
					 '  <div id="pnStorageForURL" style="display:none;">' + JSONurl + '</div>' +
	// 				 '  <div style="display:flex; justify-content:space-between;">' +
	// 				 '    <h4 id="pnPlacename" class="pntitle">' + paikannimi + '</h4>';
	// if(includeGraphLink) {
	// 	pnTemplate += '    <button id="btnSharePikagraafi" title="Jaa linkki" class="zoomButtonLast" onclick="mapcontrols_saveState(\'pikanakyma\')" data-html2canvas-ignore><img src="./img/SYKE_Map_Share.svg" class="buttonIcon"></button>';
	// }					 
	// pnTemplate += 	 '  </div>' +
	// 				 '  <h4 id="pntitle" class="pntitle"></h4>' +
					 '    <div id="pnGraafiTitleHeader" class="pnGraafiTitleHeader"></div>'+
					 '  <div id="pnGraafiContainer" class="pnGraafiContainer">' +
					 '    <div id="pnGraafi" class="pnGraafi"></div>'+
					 '    <div id="pnNyt" class="pnNyt">'+
					 '      <div id="pnNytTitle" class="pnNytTitle"></div>'+
					 '      <div id="pnNytNuoli" class="pnNytNuoli">'+
					 '        <svg version="1.1" id="arrNyt" class="arrNyt" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 303 62" style="enable-background:new 0 0 303 62;" xml:space="preserve"><style type="text/css">.arrNytShape{fill-rule:evenodd;clip-rule:evenodd;fill:#1579C6;}</style><path class="arrNytShape" id="arrNytShape" d="M301,31c0,1-34,30-34,30c-0.2-0.1-16.1-0.1-16,0H3c-1.1,0-2-0.9-2-2V3c0-1.1,0.9-2,2-2h264"/></svg>'+
					 '        <div id="pnNytValue" class="pnNytValue"></div>'+
					 '      </div>'+
					 '    </div>'+
					 '    <div id="pnEnnuste" class="pnEnnuste">'+
					 '      <div id="pnEnnusteTitle" class="pnEnnusteTitle"></div>'+
					 '      <div id="pnEnnusteNuoli" class="pnEnnusteNuoli">'+
					 '        <svg version="1.1" id="arrEnnuste" class="arrEnnuste" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 303 62" style="enable-background:new 0 0 303 62;" xml:space="preserve"><style type="text/css">.arrEnnusteShape{fill-rule:evenodd;clip-rule:evenodd;fill:#E8A655;}</style><path class="arrEnnusteShape" id="arrEnnusteShape" d="M1,31C1,30,35,1,35,1c0.2,0.1,16.1,0.1,16,0h248c1.1,0,2,0.9,2,2v56c0,1.1-0.9,2-2,2H35"/></svg>'+
					 '        <div id="pnEnnusteValue" class="pnEnnusteValue"></div>'+
					 '      </div>'+
					 '    </div>'+
					 '  </div>'+
					 '    <div id="pnGraafiTitleFooter" class="pnGraafiTitleFooter"></div>'+
					 '</div>';
	if(includeGraphLink) {
		pnTemplate = pnTemplate + '<div id="pnGraafilinkki" class="pnGraafilinkki" onclick="infoWindow_showInfoDialog()">'+
								  '  <div class="pnGraafilinkkiIkoni"></div>'+
								  '  <div class="pnGraafilinkkiTitle" id="pnGraafilinkkiTitle">Graafi</div>'+
								  '</div>';
	}
	document.getElementById(targetID).innerHTML = pnTemplate;	
	// env_addLocaleText({ element: document.getElementById('pnGraafilinkkiTitle'), localeKey: 'info_pnGraafilinkkiTitle' });
	// var linkElement = document.getElementById('pnGraafilinkki');
	// linkElement.addEventListener("click", function () {
    //     infoWindow_showInfoDialog();
    // }, false);
	var mxHeight = 250;

	/* N60 = jokien vesitilanne */
	var colorArr = getLegendColors(suure);
	var cellNum = colorArr.length;
	var cellHeight = Math.ceil(mxHeight / cellNum);
	/* Otsikko 
		getLegendTitle('N60', "2019-11-08T13:26:23+00:00");
	*/
	// document.getElementById('pntitle').innerHTML = getLegendTitle(suure, dataObj.aikaleima, false);
	if (dataObj.kuumemittari != '' && dataObj.kuumemittari != undefined) {
		document.getElementById('pnGraafiTitleHeader').innerHTML = getpnTranslation(dataObj.kuumemittari.header);
		document.getElementById('pnGraafiTitleFooter').innerHTML = getpnTranslation(dataObj.kuumemittari.footer);

		for (var i = 0; i < colorArr.length; i++) {
			var rivi = document.createElement('div');
			var tbgcol = 'background-color:' + colorArr[i] + ';';
			var theight = 'height:' + cellHeight + 'px;';
			if (i == 0) {
				rivi.setAttribute('class', 'cblock cblock-first');
			} else if (i == (colorArr.length - 1)) {
				rivi.setAttribute('class', 'cblock cblock-last');
			} else {
				rivi.setAttribute('class', 'cblock');
			}
			rivi.setAttribute('style', tbgcol + theight);
			document.getElementById('pnGraafi').appendChild(rivi);
		}
		var heijastustaso = document.createElement('div');
		heijastustaso.setAttribute('class', 'pnHeijastus');
		document.getElementById('pnGraafi').appendChild(heijastustaso);
		document.getElementById('pnNytTitle').innerHTML = getpnTranslation('Nyt') + ':<br>' + getpnTranslation(dataObj.tilanneNyt.info).toLowerCase();
		// document.getElementById('pnNytValue').innerHTML = getpnTranslation(dataObj.tilanneNyt.arvo);
		// joillekin suureille kaksi desimaalia
		let kahdenNollanSuureet = ['Merivedenkorkeus', 'Vedenkorkeus','mw','w'];
		if(kahdenNollanSuureet.indexOf(suure)!=-1) {
			document.getElementById('pnNytValue').innerHTML = dataObj.tilanneNyt.arvo.toFixed(2) + getUnit(suure);
		} else {
			document.getElementById('pnNytValue').innerHTML = dataObj.tilanneNyt.arvo + getUnit(suure);
		}
		var ennusteenpituus = '';
		ennusteenpituus = dataObj.tilanneEnnuste.enusteenpituus;
		//console.log('ennusteKK:'+ennusteKK);
		if (ennusteenpituus != '' && ennusteenpituus != undefined) {
			switch (ennusteenpituus) {
				case 14:
					ennusteenpituus = ' (2 ' + getpnTranslation('vko') + ')';
					break;
				case 30:
					ennusteenpituus = ' (1 ' + getpnTranslation('kk') + ')';
					break;
				default:
					ennusteenpituus = ' (' + ennusteenpituus + ' ' + getpnTranslation('pv') + ')';
					break;
			}
			
		}
		document.getElementById('pnEnnusteTitle').innerHTML = getpnTranslation('Ennuste') + ennusteenpituus + ':<br>' + getpnTranslation(dataObj.tilanneEnnuste.info).toLowerCase();
		// document.getElementById('pnEnnusteValue').innerHTML = getpnTranslation(dataObj.tilanneEnnuste.arvo);
		if(kahdenNollanSuureet.indexOf(suure)!=-1) {
			document.getElementById('pnEnnusteValue').innerHTML = dataObj.tilanneEnnuste.arvo.toFixed(2) + getUnit(suure);
		} else {
			document.getElementById('pnEnnusteValue').innerHTML = dataObj.tilanneEnnuste.arvo + getUnit(suure);
		}

		/* lasketaan nuolien sijainnit */
		/* maxkorkeus + borderit */
		var pxRatioH = (mxHeight + 4);
		//console.log('elementin korkeus: '+pxRatioH);

		var tMax = dataObj.kuumemittari.havaintojenMax;
		var tMin = dataObj.kuumemittari.havaintojenMin;
		//console.log('Ylä- ja alarajat: '+tMax+' ' +tMin);



		var nuoliData = sijoitaGraafiin(pxRatioH, tMax, tMin, dataObj.tilanneNyt.arvo, colorArr);
		var topval = 'top:' + nuoliData.px + 'px;';
		var colorval = 'fill:' + nuoliData.col + ';';
		//console.log('topval ' + topval);
		document.getElementById('pnNyt').setAttribute('style', topval);
		document.getElementById('arrNytShape').setAttribute('style', colorval);


		////console.log('topval luonti ' + sijoitaGraafiin(pxRatioH,tMax, tMin, dataObj.tilanneEnnuste.arvo));
		var nuoliData = sijoitaGraafiin(pxRatioH, tMax, tMin, dataObj.tilanneEnnuste.arvo, colorArr);
		var topval = 'top:' + nuoliData.px + 'px;';
		var colorval = 'fill:' + nuoliData.col + ';';
		//console.log('topval ' + topval);
		document.getElementById('pnEnnuste').setAttribute('style', topval);
		document.getElementById('arrEnnusteShape').setAttribute('style', colorval);

		/* luodaan vaikuttavuus-merkit */
		var vaikuttavuusRivi = document.createElement('div');
		vaikuttavuusRivi.setAttribute('id', 'vaikuttavuuskuvaus');
		vaikuttavuusRivi.setAttribute('class', 'vaikuttavuuskuvaus');
		document.getElementById('pnGraafiContainer').appendChild(vaikuttavuusRivi);
		var vaikuttavuusNyt;
		if (dataObj.tilanneNyt.vaikuttavuus != '' && dataObj.tilanneNyt.vaikuttavuus != undefined) {
			vaikuttavuusNyt = sijoitaVaikuttavuuspisteGraafiin(pxRatioH, tMax, tMin, dataObj.tilanneNyt.vaikuttavuus);
			if ((vaikuttavuusNyt.px != '' && vaikuttavuusNyt.px != undefined) || vaikuttavuusNyt.px == 0) {
				var vaikuttavuusTopval = 'top:' + vaikuttavuusNyt.px + 'px;';
				var vaikuttavuusDiv = document.createElement('div');
				vaikuttavuusDiv.setAttribute('style', vaikuttavuusTopval);
				vaikuttavuusDiv.setAttribute('class', 'vaikuttavuusmerkki vaikuttavuusNyt');
				let vaikuttavuusIcon = document.createElement('img');
				vaikuttavuusIcon.setAttribute('class', 'vaikuttavuusNytKuvake');
				vaikuttavuusIcon.setAttribute('src', './img/osoitin-vasen.svg');
				vaikuttavuusIcon.setAttribute('width', '58px');
				vaikuttavuusIcon.setAttribute('height', '20px');
				vaikuttavuusDiv.appendChild(vaikuttavuusIcon);
				document.getElementById('pnGraafi').appendChild(vaikuttavuusDiv);

				let vaikuttavuusIcon2 = document.createElement('img');
				vaikuttavuusIcon2.setAttribute('class', 'vaikuttavuusNytKuvake');
				vaikuttavuusIcon2.setAttribute('src', './img/osoitin-vasen.svg');
				vaikuttavuusIcon2.setAttribute('width', '58px');
				vaikuttavuusIcon2.setAttribute('height', '20px');
				document.getElementById('vaikuttavuuskuvaus').appendChild(vaikuttavuusIcon2);
				var vaikuttavuusTekstiDiv = document.createElement('div');
				vaikuttavuusTekstiDiv.setAttribute('class', 'vaikuttavuusteksti vaikuttavuustekstiNyt');
				vaikuttavuusTekstiDiv.innerHTML = getpnTranslation(vaikuttavuusNyt.info);
				document.getElementById('vaikuttavuuskuvaus').appendChild(vaikuttavuusTekstiDiv);
			}
		}
		var vaikuttavuusEnnuste;
		if (dataObj.tilanneEnnuste.vaikuttavuus != '' && dataObj.tilanneEnnuste.vaikuttavuus != undefined) {
			vaikuttavuusEnnuste = sijoitaVaikuttavuuspisteGraafiin(pxRatioH, tMax, tMin, dataObj.tilanneEnnuste.vaikuttavuus);
			if ((vaikuttavuusEnnuste.px != '' && vaikuttavuusEnnuste.px != undefined) || vaikuttavuusEnnuste.px == 0) {
				var vaikuttavuusTopval = 'top:' + vaikuttavuusEnnuste.px + 'px;';
				var vaikuttavuusDiv = document.createElement('div');
				vaikuttavuusDiv.setAttribute('style', vaikuttavuusTopval);
				vaikuttavuusDiv.setAttribute('class', 'vaikuttavuusmerkki vaikuttavuusEnnuste');
				let vaikuttavuusIcon = document.createElement('img');
				vaikuttavuusIcon.setAttribute('class', 'vaikuttavuusNytKuvake');
				vaikuttavuusIcon.setAttribute('src', './img/osoitin-oikea.svg');
				vaikuttavuusIcon.setAttribute('width', '58px');
				vaikuttavuusIcon.setAttribute('height', '20px');
				vaikuttavuusDiv.appendChild(vaikuttavuusIcon);
				document.getElementById('pnGraafi').appendChild(vaikuttavuusDiv);

				let vaikuttavuusIcon2 = document.createElement('img');
				vaikuttavuusIcon2.setAttribute('class', 'vaikuttavuusNytKuvake');
				vaikuttavuusIcon2.setAttribute('src', './img/osoitin-oikea.svg');
				vaikuttavuusIcon2.setAttribute('width', '58px');
				vaikuttavuusIcon2.setAttribute('height', '20px');
				document.getElementById('vaikuttavuuskuvaus').appendChild(vaikuttavuusIcon2);
				document.getElementById('pnGraafi').appendChild(vaikuttavuusDiv);
				var vaikuttavuusTekstiDiv = document.createElement('div');
				vaikuttavuusTekstiDiv.setAttribute('class', 'vaikuttavuusteksti vaikuttavuustekstiEnnuste');
				vaikuttavuusTekstiDiv.innerHTML = getpnTranslation(vaikuttavuusEnnuste.info);
				document.getElementById('vaikuttavuuskuvaus').appendChild(vaikuttavuusTekstiDiv);
			}
		}
		/* tuodaan näkyville */
		document.getElementById('pikanakyma').setAttribute('style', 'display:block;');
	} else {
		/* mitä näytetään jos "kuumemittari"-dataa ei ole */
	}/* dataObj.kuumemittari !=  ended */
}

/* TÄLLÄ ASEMOIDAAN NYT- TAI ENNUSTE-VAIKUTUSMARKKERIT GRAAFISSA */

function sijoitaVaikuttavuuspisteGraafiin(pxRatioH, tMax, tMin, dataValue) {
	var ret = {};
	// skipping other languages at this point
	// if (jQuery.i18n().locale == 'sv') {
	// 	return ret;
	// }
	var skaalattu = false;
	var tMaxOrig = tMax;
	var tMinOrig = tMin;
	/* jos tMin < 0, tehdään luvuista helpommin käsiteltäviä */
	if (tMin < 0) {
		var tMax = tMax + Math.abs(tMin);
		var tMin = 0;
		skaalattu = true;
	}
	//console.log('Ylä- ja alarajat, vaikuttavuuspiste: '+tMax+' ' +tMin);
	/* 19.12 */
		if(skaalattu == true){
		var datamitta = tMax + tMin;
		} else {
		var datamitta = tMax - tMin;
		}
	//console.log('Datan mitta, vaikuttavuuspiste: '+datamitta);
	/* testidata on pisteen sijainti muokatulla asteikolla */
	var piste = dataValue.raja;
	//console.log('Piste 1: '+piste);

	//no value
	if (piste == undefined || piste == -99999) {
		return ret;
	}

	/* 19.12 */
		if(skaalattu == true){
			piste = (piste + Math.abs(tMinOrig));
		} else {
			piste = (piste - Math.abs(tMinOrig));
		}
		if(piste > tMaxOrig){ 
			piste = tMax;
		}

	if (piste > tMaxOrig) {
		piste = tMax;
	}
	//console.log('Piste 2: '+piste);
	//console.log('Vaikuttavuuspiste testidata arvolle:'+ piste + ' on prosentuaalisesti ' + (piste/datamitta)*100 +' %, eli pikseleinä '+  (pxRatioH - Math.ceil(pxRatioH*(piste/datamitta))));

	ret.px = (pxRatioH - Math.ceil(pxRatioH * (piste / datamitta)));
	//console.log('Piste 3, pikseleinä: '+ ret.px);
	ret.info = dataValue.info;
	return ret;


}
/* TÄLLÄ ASEMOIDAAN NYT- TAI ENNUSTE-NUOLET GRAAFISSA */
function sijoitaGraafiin(pxRatioH, tMax, tMin, dataValue, colorArr) {
	var ret = {};
	//console.log('elementin korkeus: '+pxRatioH);
	var skaalattu = false;
	var tMinOrig = tMin;
	/* jos tMin < 0, tehdään luvuista helpommin käsiteltäviä */
	if (tMin < 0) {
		var tMax = tMax + Math.abs(tMin);
		var tMin = 0;
		skaalattu = true;
	}
	//console.log('Ylä- ja alarajat: '+tMax+' ' +tMin);
	if(skaalattu == true){
		var datamitta = tMax + tMin;
	} else {
		var datamitta = tMax - tMin;
	}

	/* testidata on pisteen sijainti muokatulla asteikolla */
	var testidata = dataValue;
	if(skaalattu == true){
			testidata = (testidata + Math.abs(tMinOrig));
	} else {
			testidata = (testidata - Math.abs(tMinOrig));
	}
	//console.log('2. testisijainti testidata arvolle:'+ testidata + ' on prosentuaalisesti ' + (testidata/datamitta)*100 +' %, eli pikseleinä '+  (pxRatioH - Math.ceil(pxRatioH*(testidata/datamitta))));
	ret.px = (pxRatioH - Math.ceil(pxRatioH * (testidata / datamitta)));

	/* haetaan väri, prosenttiosuuden perusteella */
	var colNum = colorArr.length;
	//console.log('värin sijainti prosenttiosuuden perusteella: ' +  Math.ceil((((100 - (testidata/datamitta)*100))/100)*colNum) + ' väri: ' + colorArr[Math.floor((((100 - (testidata/datamitta)*100))/100)*colNum)]);
	ret.col = colorArr[Math.floor((((100 - (testidata / datamitta) * 100)) / 100) * colNum)];
	return ret;
}
/* TÄLLÄ SYÖTETÄÄN TEEMOJEN LEGENDOJEN VÄRISETTI */
function getLegendColors(layerName) {
	var ret = [];
	switch (layerName) {
		case 'N60':  //DEMO
			ret = ['#2d0690', '#572bc5', '#1579c6', '#02bcfc', '#54d1fc', '#fcdeb9', '#e8a655'];
			break;
		case 'Virtaama':
		case 'q':
			ret = ['#2d0690', '#572bc5', '#1579c6', '#02bcfc', '#54d1fc', '#fcdeb9', '#e8a655'];
			break;
		case 'Vedenkorkeus':
		case 'w':
			ret = ['#2D0690', '#572BC5', '#02BCFC', '#54D1FC', '#FCDEB9', '#E8A655', '#997D56'];
			break;
		case 'Merivedenkorkeus':
		case 'mw':
			ret = ['#2D0690', '#572BC5', '#02BCFC', '#54D1FC', '#FCDEB9', '#E8A655', '#997D56'];
			break;
		case 'Pohjavedenkorkeus':
		case 'gv':
			ret = ['#2D0690', '#572BC5', '#022BC5', '#54D1FC', '#FCDEB9', '#E8A655', '#997D56'];
			break;
		case 'Maankosteus':
			ret = ['#996F56', '#E8A655', '#FCDEB9', '#54D1FC', '#02BCFC', '#572BC5', '#2d0690'];
			break;
		case 'Jäänpaksuus':
		case 'ice':
			ret = ['#012CAC', '#005489', '#2678B7', '#5097CD', '#02BCFC', '#54D1FC', '#A6E6FC', '#D2F0FA'];
			break;
		case 'Veden lämpötila':
		case 'tw':
			ret = ['#E8A655', '#FDC37A', '#FCDEB9', '#A6E6FC', '#54D1FC', '#5097CD', '#2678B7', '#005489'];
			break;
		case 'Lumen vesiarvo':
		case 'snow':
			ret = ['#39086C', '#2D0690', '#572BC5', '#1579C6', '#02BCFC', '#54D1FC', '#A6E6FC', '#D2F0FA'];
			break;
		default:
			break;
	}
	return ret;
}
/* TÄLLÄ SYÖTETÄÄN TEEMAN MUKAINEN OTSIKKO + PÄIVÄYS JSON-TIEDOSTOSTA */
function getLegendTitle(layerName, datadate, includeTime=true) {
	var ret = '';
	tDate = new Date(datadate);
	////console.log((tDate.getMonth()+1))
	var strDate = tDate.getDate() + '.' + (tDate.getMonth() + 1) + '. ' + getpnTranslation('klo') + ' ' + tDate.getHours() + ':' + ('0' + tDate.getMinutes()).substr(-2);
	if(!includeTime) {
		strDate = tDate.getDate() + '.' + (tDate.getMonth() + 1) + '. ';
	}
	switch (layerName) {
		case 'N60': // DEMO
			ret = getpnTranslation('Jokien vesitilanne') + ' ' + strDate
			break;
		case 'Virtaama':
		case 'q':
			ret = getpnTranslation('featureInfoObservationPointValues_WaterFlow') + ' ' + strDate;
			break;
		case 'Vedenkorkeus':
		case 'w':
			ret = getpnTranslation('layernames_waterLevelLakes') + ' ' + strDate;
			break;
		case 'Merivedenkorkeus':
		case 'mw':
			ret = getpnTranslation('layernames_waterLevelLakes') + ' ' + strDate;
			break;
		case 'Pohjavedenkorkeus':
			case 'gv':
			ret = getpnTranslation('layernames_groundWaterLevel') + ' ' + strDate;
			break;
		case 'Maankosteus':
			ret = getpnTranslation('layernames_soilHumidity') + ' ' + strDate;
			break;
		case 'Jäänpaksuus':
		case 'ice':
			ret = getpnTranslation('layernames_iceThicknessLakes') + ' ' + strDate;
			break;
		case 'Veden lämpötila':
		case 'tw':
			ret = getpnTranslation('layernames_surfaceTemperatureLakes') + ' ' + strDate;
			break;
		case 'Lumen vesiarvo':
		case 'snow':
			ret = getpnTranslation('layernames_snowWaterContent') + ' ' + strDate;
			break;
		default:
				ret = getpnTranslation(layerName) + ' ' + strDate;
			break;
	}
	return ret;
}
/* TÄLLÄ HAETAAN YKSIKKÖ KULLEKIN SUUREELLE */
function getUnit(suure) {
	var ret = '';
	switch (suure) {
		case 'N60':  //DEMO
			ret = '';
			break;
		case 'Virtaama':
		case 'q':
			ret = ' m<sup>3</sup>/s';
			break;
		case 'Vedenkorkeus':
		case 'w':
			ret = ' m';
			break;
		case 'Merivedenkorkeus':
		case 'mw':
			ret = ' m';
			break;
		case 'Pohjavedenkorkeus':
			case 'gv':
			ret = ' m';
			break;
		case 'Maankosteus':
			ret = ' mm';
			break;
		case 'Jäänpaksuus':
		case 'ice':
			ret = ' cm';
			break;
		case 'Veden lämpötila':
		case 'tw':
			ret = 'C&deg;';
			break;
		case 'Lumen vesiarvo':
		case 'snow':
			ret = 'kg/m<sup>2</sup>';
			break;
		default:
			break;
	}
	return ret;
}
function getDataObj(whateverUrl) {
	return new Promise(function (resolve, reject) {
		var Httpreq = new XMLHttpRequest(); // a new request
		Httpreq.open("GET", whateverUrl);
		// Httpreq.send(null);
		// return Httpreq.responseText;
		Httpreq.timeout = 5000;
		Httpreq.onload = () => {
			if (Httpreq.status == 200) {
				resolve(Httpreq.responseText);
			} else {
				reject('[tee_pika_graafi.getDataObj] Failed to retrieve pikanäkymä-json: ' + Httpreq.statusText);
			}
		}
		Httpreq.onerror = () => {
			reject('[tee_pika_graafi.getDataObj] Network error getting pikanäkymä-json');
		}
		Httpreq.send();
	});
}
/* TÄLLÄ VAIHDETAAN KÄÄNNÖS ANNETTUUN TEKSTIIN */
function getpnTranslation(txt) {
	// tähän käännöksen ujuttaminen
	// return jQuery.i18n(txt);
	return txt;

}