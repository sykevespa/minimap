monitoringPoints = [];

var forecastUrl = 'https://geoserver2.ymparisto.fi/geoserver/vespa/wms?x';
var vesidataUrl = 'https://wwwi2.ymparisto.fi/i2/vespa/';
var sykeProxy = 'https://paikkatieto.ymparisto.fi/proxy/proxy.ashx';
// for development env
try {  // fails on older browsers
    if (window.location.hostname.indexOf('127.0.0.1')!=-1) {
        sykeProxy = 'http://kkgeoct1.env.fi/proxy/proxy.ashx';
        vesidataUrl = 'https://wwwi2.ymparisto.fi/i2/vespa/development/';
    } else {
        // forecastUrl = sykeProxy + '?' + forecastUrl;
    }
} catch(err) { 
    // forecastUrl = sykeProxy + '?' + forecastUrl;
}
try {  // fails on older browsers
    if (window.location.href.includes('/dev/')) {
        vesidataUrl = 'https://wwwi2.ymparisto.fi/i2/vespa/development/';
        backendURL = sykeProxy + '?' + 'http://testbed.ymparisto.fi/VespaWebAPI/api/';
    }
} catch(err) {}


function initialize() {
    
    // input field autocomplete style
    jQuery.extend(jQuery.ui.autocomplete.prototype.options, {
        open: function (event, ui) {
            jQuery(this).autocomplete("widget").css({
                "width": (jQuery(this).width() + "px")
            });
        }
    });

    // **** ASYNC DATA TO WAIT FOR ****
    let promiseArray = []; 
    var searchElement = document.getElementById('searchField');
    searchElement['placeholder'] = 'Odota, ladataan tietoja...';
    searchElement['placeholder'].disabled = true;

    // monitoring points
    promiseArray.push(
        new Promise(function (resolve, reject) {
            try {
                let monitoringPointsURL = vesidataUrl + 'MonitoringPoints.json';
                jQuery.ajax(monitoringPointsURL, {
                    type: "GET",
                    dataType: "json",
                    // ,contentType: "application/json; charset=utf-8",
                    timeout: 3000
                }).then(data => {
                    if(data['monitoringPoints']) {
                        monitoringPoints = data['monitoringPoints'];
                        console.log('  ...loaded monitoring points from ' + monitoringPointsURL);
                    } else {
                        // pick whatever is first
                        for (key in data) {
                            if(Array.isArray(data[key])) {
                                monitoringPoints = data[key];
                                console.log('  ...loaded monitoring points from ' + monitoringPointsURL);
                            } else {
                                console.log('  [ERROR] Getting monitoring points from ' + monitoringPointsURL + ' failed: could not find the array');
                            }
                            break;
                        }
                    }
                    resolve({ target: 'monitoringPoints', success: true });
                }).fail((jqXHR, textStatus, errorThrown) => {
                    console.log('  [ERROR] Loading monitoring points from ' + monitoringPointsURL + ' failed: ' + errorThrown);
                    resolve({ target: 'monitoringPoints', success: false });
                });        
            } catch (error) {
                console.error('  [ERROR]  Getting monitoring points failed: ' + error.toString());
                resolve({ target: 'monitoringPoints', success: false });
            } 
        })
    ); 
    
    // communities list
    promiseArray.push(loadCommunities());

    Promise.all(promiseArray).then(function(successArray){
        searchElement['placeholder'] = 'Hae paikannimellä';
        searchElement['placeholder'].disabled = false;
        // let errorMessage = jQuery.i18n('notification_problemsLoadingEnvironment');
        let errorMessage = 'Errors when loading some data:';
        let errors = false;
        for (let i=0; i<successArray.length; i++) {
            if(!successArray[i].success) {
                errors=true;
                errorMessage = errorMessage + ' [' + successArray[i].target + ']';
            }
        }
        if(errors) {
            alert(errorMessage);
        }
    }).catch(function(exception) {
        alert('Failed to load datasets');
        console.log(exception);
    });


    // autocomplete action
    //jQuery.support.cors = true;
    jQuery("#searchField").autocomplete({
        source: getPlaceNames,
        select: function (event, ui) {
            handleSearchItem(ui.item);
        },
        response: function (event, ui) {
            jQuery("#searchField").autocomplete("option", "hitCount", ui.content.length);
        },
        hitCount: 10,
        delay: 500,
        close: function() {
            jQuery('#searchField').autocomplete('close');
        }
    }).data('uiAutocomplete')._renderItem = function (ul, item) {
        if(item.icon) {
            return jQuery('<li />')
                .data('item.autocomplete', item)
                .append('<a>' + item.label + '</a><img style="margin-left:5px;" src="' + item.icon + '" />')
                .appendTo(ul);
        } else {
            return jQuery('<li />')
                .data('item.autocomplete', item)
                .append('<a>' + item.label + '</a>')
                .appendTo(ul);
        }
    };

}




function handleSearchItem(searchItem) {
    let coordinates = [];
    if (searchItem.MMLPlaceID) {  // from MML service
        coordinates = [parseFloat(searchItem.coordX), parseFloat(searchItem.coordY)];
    } else {   // location from monitoring points
        coordinates = searchItem.geometry;
    }
    // open real page
    let nextURL = './index.html?x=' + coordinates[0] + '&y=' + coordinates[1] + '&name=' + searchItem.label;
    window.open(nextURL, '_self');
}


