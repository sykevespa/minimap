
var MMLMunicipalities = {};
var MMLtypeBackupURL = 'https://www.vesi.fi/karttapalvelu/externs/';


function loadCommunities() {   

    function getCommunitiesBackup(resolve) {
        try {
            let strUrl = MMLtypeBackupURL + 'kunnnat.xsd';
            MMLMunicipalities = {};
            jQuery.ajax({
                url: strUrl,
                dataType: "xml",
                data: '',
                timeout: 2000,
                success: function (dataXml) {
                    // parse from XML
                    var placeTypes = dataXml.getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'enumeration');
                    if (placeTypes && placeTypes.length > 0) {
                        for (var i = 0; i < placeTypes.length; i++) {
                            MMLMunicipalities[placeTypes[i].getAttribute('value')] = {};
                            var annotations = placeTypes[i].getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'annotation');
                            var documentations = annotations[0].getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'documentation');
                            for (var j = 0; j < documentations.length; j++) {
                                switch (documentations[j].getAttribute('xml:lang')) {
                                    case 'fin':
                                        MMLMunicipalities[placeTypes[i].getAttribute('value')]['fi'] = documentations[j].textContent;
                                        MMLMunicipalities[placeTypes[i].getAttribute('value')]['sv'] = documentations[j].textContent;
                                        break;
                                    case 'swe':
                                        MMLMunicipalities[placeTypes[i].getAttribute('value')]['se'] = documentations[j].textContent;
                                        if (MMLMunicipalities[placeTypes[i].getAttribute('value')]['fi'] == undefined) {
                                            MMLMunicipalities[placeTypes[i].getAttribute('value')]['fi'] = documentations[j].textContent;
                                        }
                                        break;
                                    case 'eng':
                                        MMLMunicipalities[placeTypes[i].getAttribute('value')]['en'] = documentations[j].textContent;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        console.log('  ...loaded MML municipalities (' + placeTypes.length + ')');
                        resolve({ target: 'MML kunnat', success: true });
                    } else {
                        console.error('  [ERROR] Loading MML municipalities from LOCAL failed: received 0 types');
                        resolve({ target: 'loading MML municipalities', success: false });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.error('  [ERROR] Loading MML municipalities from LOCAL failed (ajax error), status ' + xhr.status + ', ' + thrownError);
                    resolve({ target: 'loading MML municipalities', success: false });
                }
            });            
        } catch (error) {
            console.error('  [ERROR] Parsing MML municipalities from LOCAL failed: ' + error.toString());
            resolve({ target: 'parsing MML municipalities', success: false });
        } 
    }

    return new Promise(function (resolve, reject) {
        try {
            //strUrl = '../externs/kunta.xsd';
            var strUrl = sykeProxy + '?' + 'http://xml.nls.fi/Nimisto/Nimistorekisteri/kunta.xsd';
            MMLMunicipalities = {};
            jQuery.ajax({
                url: strUrl,
                dataType: "xml",
                data: '',
                timeout: 3000,
                success: function (dataXml) {
                    // parse from XML
                    var placeTypes = dataXml.getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'enumeration');
                    if (placeTypes && placeTypes.length > 0) {
                        for (var i = 0; i < placeTypes.length; i++) {
                            MMLMunicipalities[placeTypes[i].getAttribute('value')] = {};
                            var annotations = placeTypes[i].getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'annotation');
                            var documentations = annotations[0].getElementsByTagNameNS('http://www.w3.org/2001/XMLSchema', 'documentation');
                            for (var j = 0; j < documentations.length; j++) {
                                switch (documentations[j].getAttribute('xml:lang')) {
                                    case 'fin':
                                        MMLMunicipalities[placeTypes[i].getAttribute('value')]['fi'] = documentations[j].textContent;
                                        MMLMunicipalities[placeTypes[i].getAttribute('value')]['sv'] = documentations[j].textContent;
                                        break;
                                    case 'swe':
                                        MMLMunicipalities[placeTypes[i].getAttribute('value')]['se'] = documentations[j].textContent;
                                        if (MMLMunicipalities[placeTypes[i].getAttribute('value')]['fi'] == undefined) {
                                            MMLMunicipalities[placeTypes[i].getAttribute('value')]['fi'] = documentations[j].textContent;
                                        }
                                        break;
                                    case 'eng':
                                        MMLMunicipalities[placeTypes[i].getAttribute('value')]['en'] = documentations[j].textContent;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        console.log('  ...loaded MML municipalities (' + placeTypes.length + ')');
                        resolve({ target: 'MML kunnat', success: true });
                    } else {
                        console.error('  [ERROR] Loading MML municipalities failed: received 0 types');
                        // resolve({ target: 'loading MML municipalities', success: false });
                        getCommunitiesBackup(resolve)
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.error('  [ERROR] Loading MML municipalities failed (ajax error), status ' + xhr.status + ', ' + thrownError);
                    // resolve({ target: 'loading MML municipalities', success: false });
                    getCommunitiesBackup(resolve)
                }
            });            
        } catch (error) {
            console.error('  [ERROR] Parsing MML municipalities failed: ' + error.toString());
            // resolve({ target: 'parsing MML municipalities', success: false });
            getCommunitiesBackup(resolve)
        } 
    });   
}





/**
 * Function providing the list for the jQuery UI autocomplete.
 * The function combines applicable targets from SYKE monitoring 
 * point JSON file ("localData") and the results from a query to
 * a name service (currently GeoNames).
 * 
 * Parameters as specified for jQuery UI autocomplete source-function:
 *  - request: the request received from the input element, with field 
 *      "term" containing the search term
 *  - response: the callback function to which a ready array of 
 *      results is given
 */
async function getPlaceNames(request, response) {
    let nameListPromise = getPlaceNameList(request.term);
    let nameList = [];
    try {
        nameList = await nameListPromise;
    } catch (err) { }
    response(nameList);    
}




function getPlaceNameList(requestTerm) {
    return new Promise(function (resolve, reject) {
        try {
            var localdata = []; 
            placenamesarray = [];
        
            //monitoring points first:
            // var filteredData = pointData['monitoringPoints'].features.filter(function(monitoringPoint) {
            var filteredData = monitoringPoints.filter(function(monitoringPoint) {
                return (monitoringPoint.name.toLowerCase().indexOf(requestTerm.toLowerCase())!=-1);
            });
            filteredData.forEach(function (element) {
                element.label = element.name.toString();
            });
            localdata = jQuery.extend(true, localdata, filteredData);        
        
            //then MML Nimihaku:
            if (requestTerm.length>2) {
                //  var strUrl = sykeProxy + '?https://ws.nls.fi/nimisto/wfs?SERVICE=WFS&REQUEST=GetFeature&VERSION=1.1.0&NAMESPACE=xmlns(pnr=http://xml.nls.fi/Nimisto/Nimistorekisteri/2009/02)&TYPENAME=pnr:Paikannimi&Filter=<Filter><PropertyIsLike matchCase="false" wildCard="*" singleChar="?" escapeChar="!"><PropertyName>pnr:kirjoitusasu</PropertyName><Literal>#PAIKKA#</Literal></PropertyIsLike></Filter>&SRSNAME=#EPSG#&MAXFEATURES=#MAXROWS#&RESULTTYPE=results';
                var strUrl = 'https://ws.nls.fi/nimisto/wfs?SERVICE=WFS&REQUEST=GetFeature&VERSION=1.1.0&NAMESPACE=xmlns(pnr=http://xml.nls.fi/Nimisto/Nimistorekisteri/2009/02)&TYPENAME=pnr:Paikannimi&Filter=%3CFilter%3E%3CPropertyIsLike%20matchCase=%22false%22%20wildCard=%22*%22%20singleChar=%22?%22%20escapeChar=%22!%22%3E%3CPropertyName%3Epnr:kirjoitusasu%3C/PropertyName%3E%3CLiteral%3E' +
                    requestTerm + '*%3C/Literal%3E%3C/PropertyIsLike%3E%3C/Filter%3E&SRSNAME=EPSG:3067&MAXFEATURES=50&RESULTTYPE=results&sortBy=pnr:mittakaavarelevanssiKoodi+D';
                strUrl = sykeProxy + '?' + encodeURIComponent(strUrl);
                // var strUrl = '../MML_nimihaun_tulos.xml';
        
                jQuery.ajax({
                    url: strUrl,
                    dataType: "xml",
                    data: '',
                    success: function (dataXml) {
                        // parse from XML
                        var pnr = 'http://xml.nls.fi/Nimisto/Nimistorekisteri/2009/02';
                        var features = dataXml.getElementsByTagNameNS('http://www.opengis.net/gml', 'featureMember');
                        if (features && features.length > 0) {
                            var ajaxData = [];
                            for (var i = 0; i < features.length; i++) {
                                var paikkaID = '-999';
                                var feature = features[i].getElementsByTagNameNS(pnr, 'Paikannimi')[0];
                                paikkaID = feature.getElementsByTagNameNS(pnr, 'paikkaID')[0].textContent;
                                var paikkatyyppiKoodi = feature.getElementsByTagNameNS(pnr, 'paikkatyyppiKoodi')[0].textContent;
                                var kuntaKoodi = feature.getElementsByTagNameNS(pnr, 'kuntaKoodi')[0].textContent;
                                var kirjoitusasu = feature.getElementsByTagNameNS(pnr, 'kirjoitusasu')[0].textContent;
                                var paikkaSijainti = feature.getElementsByTagNameNS(pnr, 'paikkaSijainti')[0];
                                var pos = paikkaSijainti.getElementsByTagNameNS('http://www.opengis.net/gml','pos')[0].textContent;
                                var coords = pos.split(' ');
                                // var placeType = env_getMMLPlaceType(paikkatyyppiKoodi);
                                // if(placeType && (placeType.toLowerCase()=='vakavesi' || placeType.toLowerCase()=='virtavesi')) {
                                //     placeType = undefined;
                                // }
                                var municipality = getMMLMunicipality(kuntaKoodi);
                                var prio = convertToPriorityValue(paikkatyyppiKoodi);
                                var paikkanimi;
                                if (municipality && municipality.toLowerCase()!=kirjoitusasu.toLowerCase()) {
                                    paikkanimi = municipality ? kirjoitusasu + ', ' + municipality : kirjoitusasu;
                                } else {
                                    paikkanimi = kirjoitusasu;
                                    if (kirjoitusasu.toLowerCase() == requestTerm.toLowerCase()) {
                                        prio = 0;  // exact match with municipality
                                    }
                                }
                                // paikkanimi = placeType ? paikkanimi + ' (' + placeType + ')' : paikkanimi; 
                                ajaxData.push({
                                    MMLPlaceID: paikkaID,
                                    label: paikkanimi,
                                    kirjoitusasu: kirjoitusasu,
                                    paikkatyyppiKoodi: paikkatyyppiKoodi,
                                    kuntaKoodi: kuntaKoodi,
                                    coordX: coords[0],
                                    coordY: coords[1],
                                    priority: prio
                                });
                            }
                            ajaxData = prioritizeMMLplaces(ajaxData);
                            // ajaxData = prioritizeExactMatches(ajaxData, request.term);
                            ajaxData = ajaxData.slice(0, 10);
                            placenamesarray = jQuery.extend(true, placenamesarray, ajaxData);
                            // resolve(localdata.concat(ajaxData));
                            resolve(localdata.concat(ajaxData));
                        } else {
                            resolve(localdata);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log('MML name service call failed: ' + xhr.responseText);
                        resolve(localdata);
                    }
                });
            } else {
                resolve(localdata);
            }
        } catch(err) {
            console.error('Failed promise: ' + err);
            resolve(localdata);
        }
    });
}


function convertToPriorityValue(placeTypeID) {
    switch (placeTypeID) {
        case '410':
            return 1;
            break;
        case '420':
            return 2;
            break;
        case '415':
            return 3;
            break;
        case '425':
            return 4;
            break;
        case '540':
            return 5;
            break;
        case '560':
            return 6;
            break;
        case '120':
            return 7;
            break;
        case '590':
            return 8;
            break;    
        default:
            return 99;
            break;
    }
}


function prioritizeMMLplaces(placeArray) {
    //first remove certain placetypes and obvious duplicates
    // var typesOfInterest = ['120','330','345','350','410','415','420','425','435','540','550','560','590','604']
    var typesOfInterest = ['410','420','540','550','560']
    for (var i = placeArray.length - 1; i>-1; i--) {
        // var currentPlace = placeArray[i];
        if (!typesOfInterest.includes(placeArray[i]['paikkatyyppiKoodi'])) {
            placeArray.splice(i, 1);
            continue;
        }
        for(var j = 0; j<placeArray.length; j++) {
            if (i != j && placeArray[i]['kirjoitusasu'] == placeArray[j]['kirjoitusasu'] && placeArray[i]['kuntaKoodi'] == placeArray[j]['kuntaKoodi'] && placeArray[i]['paikkatyyppiKoodi'] == placeArray[j]['paikkatyyppiKoodi']) {
                placeArray.splice(i,1);
                break;
            }
        }
    }
    //next sort by priority
    placeArray.sort(placeCompareFunction);

    return placeArray;
}


function placeCompareFunction(a, b) {
    if (a.priority < b.priority) {
        return -1;
    }
    if (a.priority > b.priority) {
        return 1;
    }
    return 0;
}


// function prioritizeExactMatches(placeArray, searchTerm) {
//     var newArray = [];
//     for (var j = 0; j < placeArray.length; j++) {
//         if (placeArray[j]['kirjoitusasu'].toLowerCase() == searchTerm.toLowerCase()) {
//             newArray.unshift(placeArray[j]);
//         } else {
//             newArray.push(placeArray[j]);
//         }
//     }
//     return newArray;
// }


function getMMLMunicipality(municipalityID) {
    try {
        if (MMLMunicipalities.hasOwnProperty(municipalityID)) {
            // var currentLocale = jQuery.i18n().locale;
            let currentLocale = 'fi';
            return MMLMunicipalities[municipalityID][currentLocale];
        } else {
            return null;
        } 
    } catch {
        return null;
    }
}

