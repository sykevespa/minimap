
monitoringPoints = [];
theSearchItem = {};

var forecastUrl = 'https://geoserver2.ymparisto.fi/geoserver/vespa/wms?x';
var vesidataUrl = 'https://wwwi2.ymparisto.fi/i2/vespa/';
var sykeProxy = 'https://paikkatieto.ymparisto.fi/proxy/proxy.ashx';
// for development env
try {  // fails on older browsers
    if (window.location.hostname.indexOf('127.0.0.1')!=-1) {
        sykeProxy = 'http://kkgeoct1.env.fi/proxy/proxy.ashx';
        vesidataUrl = 'https://wwwi2.ymparisto.fi/i2/vespa/development/';
    } else {
        // forecastUrl = sykeProxy + '?' + forecastUrl;
    }
} catch(err) { 
    // forecastUrl = sykeProxy + '?' + forecastUrl;
}
try {  // fails on older browsers
    if (window.location.href.includes('/dev/')) {
        vesidataUrl = 'https://wwwi2.ymparisto.fi/i2/vespa/development/';
        backendURL = sykeProxy + '?' + 'http://testbed.ymparisto.fi/VespaWebAPI/api/';
    }
} catch(err) {}

var basemap;

var mapLayers = {
    'finnpxxx': {
        title: 'layernames_accumulatedRain',
        layer: 'vespa:finnpxxx_absoluteValue',
        pointObservationType: ['fmiPrecPoints'],
        monitoringPointType: []
    },
    'finnqrxx': {
        title: 'layernames_waterLevelRivers',
        layer: 'vespa:finnqrxx_absoluteValue',
        pointObservationType: [],
        // monitoringPointType: ['w', 'q', 'mw', 'sw']
        monitoringPointType: ['q']
    },
    'finnsmia': {
        title: 'layernames_soilHumidity',
        layer: 'vespa:finnsmia_absoluteValue',
        pointObservationType: [],
        monitoringPointType: []
    },
    'finngvaj': {
        title: 'layernames_groundWaterLevel',
        layer: 'vespa:finngvaj_absoluteValue',
        pointObservationType: [],
        // monitoringPointType: ['Pohjavedenkorkeus', 'gv']
        monitoringPointType: ['gv']
    },
    'finnwxxx': {
        title: 'layernames_waterLevelLakes',
        layer: 'vespa:finnwxxx_absoluteValue',
        pointObservationType: ['fmiWATLEVPoints'],
        // monitoringPointType: ['w', 'mw', 'sw']
        monitoringPointType: ['w']
    },
    'finnhice': {
        title: 'layernames_iceThicknessLakes',
        layer: 'vespa:finnhice_absoluteValue',
        pointObservationType: ['icePoints'],
        monitoringPointType: ['ice']
    },
    'finntwlx': {
        title: 'layernames_surfaceTemperatureLakes',
        layer: 'vespa:finntwlx_absoluteValue',
        pointObservationType: ['lakeTempPoints'],
        monitoringPointType: ['tw']
    },
    'finnlumi': {
        title: 'layernames_snowWaterContent',
        layer: 'vespa:finnlumi_absoluteValue',
        pointObservationType: ['swePoints'],
        monitoringPointType: ['snow']
    },
    'finnlupy': {
        title: 'layernames_snowDepth',
        layer: 'vespa:finnlupy_absoluteValue',
        pointObservationType: ['snowDepthPoints'],
        monitoringPointType: []
    },
    'finnrouy': {
        title: 'layernames_soilFreeze',
        layer: 'vespa:finnrouy_absoluteValue',
        pointObservationType: ['groundFrostPoints'],
        monitoringPointType: []
    }
}

var pointData = {
    // 'monitoringPoints': {
    //     'title': 'monitoringPoints',
    //     'features': [],
    //     'featuresUrl': vesidataUrl + 'MonitoringPoints.json'
    // },
    'fmiWATLEVPoints': {
        'title': 'fmiWATLEVPoints',
        'features': [],
        'featuresUrl': vesidataUrl + 'WATLEV.json'
    },
    'fmiPrecPoints': {
        'title': 'fmiPrecPoints',
        'features': [],
        'featuresUrl': vesidataUrl + 'rrday.json'
    },
    'snowDepthPoints': {
        'title': 'snowDepthPoints',
        'features': [],
        'featuresUrl': vesidataUrl + 'snow.json'
    },
    'swePoints': {
        'title': 'swePoints',
        'features': [],
        'featuresUrl': vesidataUrl + 'snowObservations.json'
    },
    'lakeTempPoints': {
        'title': 'lakeTempPoints',
        'features': [],
        'featuresUrl': vesidataUrl + 'lakeTemperatureObservations.json'
    }
    ,'groundFrostPoints': {
        'title': 'groundFrostPoints',
        'features': [],
        'featuresUrl': vesidataUrl + 'groundFrostObservations.json'
    }
    ,'icePoints': {
        'title': 'icePoints',
        'features': [],
        'featuresUrl': vesidataUrl + 'iceObservations.json'
    }
}

var URLParams = readURLParameters();

// define ETRS-TM35FIN for OpenLayers
ol.proj.setProj4(proj4);
proj4.defs("EPSG:3067", "+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
//ol.proj.proj4.register(proj4);
var projection = new ol.proj.Projection({
    code: 'EPSG:3067',
    // extent: [-3669433.90, 4601644.86, 648181.26, 9364104.12]
    extent: [-1250000, 6300000, 2200000, 8300000]
});
// proj4.defs('EPSG:3067", "+proj=utm +zone=35 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
// ol.proj.proj4.register(proj4);



function initialize() {
    
    // get MML WMTS properties (in the background)
    var url = sykeProxy + '?https://karttakuva.maanmittauslaitos.fi/maasto/wmts?SERVICE=WMTS&REQUEST=GetCapabilities&VERSION=1.0.0';
    var req = new XMLHttpRequest()
    req.open('GET', url)
    req.timeout = 20000
    req.onload = function () {
        if (req.status === 200) {
            console.log('Lo to get WMTS capabilities from ' + url);
            initiateMMLBasemap(req);
        } else {
            console.log('Failed to get WMTS capabilities from ' + url);
        }
    }
    req.onerror = () => {
        console.log('Failed to get WMTS capabilities from ' + url);
    }
    req.send();    

    // input field autocomplete style
    jQuery.extend(jQuery.ui.autocomplete.prototype.options, {
        open: function (event, ui) {
            jQuery(this).autocomplete("widget").css({
                "width": (jQuery(this).width() + "px")
            });
        }
    });

    console.log('Loading locales...');
    var localesList = {
        'en': './locales/en.json',
        'fi': './locales/fi.json',
        'sv': './locales/sv.json'
    };
    jQuery.i18n().load(localesList).done(function () { 
        console.log('...locales loaded!') 
        // language
        if (URLParams['lang'] && localesList.hasOwnProperty(URLParams['lang'])) {
            jQuery.i18n().locale = URLParams['lang'];
        } else if(window.lang && localesList.hasOwnProperty(window.lang)) {
            jQuery.i18n().locale = window.lang;
        } else {
            jQuery.i18n().locale = 'fi';
        }
        jQuery('body').i18n();
        continueInit();
    });
}

function continueInit() {

    let selectTheme = document.getElementById("selectTheme");
    for(theme in mapLayers) {
        let option = document.createElement("option");
        option.label = jQuery.i18n(mapLayers[theme].title);
        option.value = theme;
        selectTheme.add(option);
    }
    selectTheme.selectedIndex=0;
    selectTheme.addEventListener("change", function (event) {
        selectTheme_changed();
    }, false);
    let selectNearbypoint = document.getElementById('selectNearbypoint');    
    selectNearbypoint.addEventListener("change", function () {
        selectNearbypoint_change();
    }, false);
    let selectNearbypoint2 = document.getElementById('selectNearbypoint2');    
    selectNearbypoint2.addEventListener("change", function () {
        selectNearbypoint2_change();
    }, false);
    let selectObsType = document.getElementById('selectObsType');    
    selectObsType.addEventListener("change", function () {
        selectObsType_change();
    }, false);


    // **** ASYNC DATA TO WAIT FOR ****
    let promiseArray = []; 
    var searchElement = document.getElementById('searchField');
    searchElement['placeholder'] = 'Odota, ladataan tietoja...';
    searchElement['placeholder'].disabled = true;

    // monitoring points
    promiseArray.push(
        new Promise(function (resolve, reject) {
            try {
                let monitoringPointsURL = vesidataUrl + 'MonitoringPoints.json';
                jQuery.ajax(monitoringPointsURL, {
                    type: "GET",
                    dataType: "json",
                    // ,contentType: "application/json; charset=utf-8",
                    timeout: 3000
                }).then(data => {
                    if(data['monitoringPoints']) {
                        monitoringPoints = data['monitoringPoints'];
                        console.log('  ...loaded monitoring points from ' + monitoringPointsURL);
                    } else {
                        // pick whatever is first
                        for (key in data) {
                            if(Array.isArray(data[key])) {
                                featLayer.features = data[key];
                                console.log('  ...loaded monitoring points from ' + monitoringPointsURL);
                            } else {
                                console.log('  [ERROR] Getting monitoring points from ' + monitoringPointsURL + ' failed: could not find the array');
                            }
                            break;
                        }
                    }
                    resolve({ target: 'MonitoringPoints', success: true });
                }).fail((jqXHR, textStatus, errorThrown) => {
                    console.log('  [ERROR] Loading monitoring points from ' + monitoringPointsURL + ' failed: ' + errorThrown);
                    resolve({ target: 'MonitoringPoints', success: false });
                });        
            } catch (error) {
                console.error('  [ERROR]  Getting monitoring points failed: ' + error.toString());
                resolve({ target: 'MonitoringPoints', success: false });
            } 
        })
    ); 

    // point data
    for (layerID in pointData) {            
        if (pointData.hasOwnProperty(layerID) && pointData[layerID].featuresUrl) {
            promiseArray.push(
                new Promise(function (resolve, reject) {
                    try {
                        let featLayer = pointData[layerID];
                        jQuery.ajax(featLayer.featuresUrl, {
                            type: "GET",
                            dataType: "json",
                            // ,contentType: "application/json; charset=utf-8",
                            timeout: 3000
                        }).then(data => {
                            if(data['monitoringPoints']) {
                                featLayer.features = data['monitoringPoints'];
                                console.log('  ...loaded point data for ' + featLayer.title + ' from ' + featLayer.featuresUrl);
                            } else {
                                // pick whatever is first
                                for (key in data) {
                                    if(Array.isArray(data[key])) {
                                        featLayer.features = data[key];
                                        console.log('  ...loaded point data for ' + featLayer.title + ' from ' + featLayer.featuresUrl);
                                    } else {
                                        console.log('  [ERROR] Getting point data for ' + featLayer.title + ' from ' + featLayer.featuresUrl + ': could not find the array');
                                    }
                                    break;
                                }
                            }
                            resolve({ target: featLayer.title, success: true });
                        }).fail((jqXHR, textStatus, errorThrown) => {
                            console.log('  [ERROR] Loading point data for ' + featLayer.title + ' from ' + featLayer.featuresUrl + ' failed: ' + errorThrown);
                            resolve({ target: featLayer.title, success: false });
                        });        
                    } catch (error) {
                        console.error('  [ERROR]  Getting point data failed: ' + error.toString());
                        resolve({ target: featLayer.title, success: false });
                    } 
                })
            ); 
        }
    }

    // communities list
    promiseArray.push(loadCommunities());

    Promise.all(promiseArray).then(function(successArray){
        searchElement['placeholder'] = 'Hae paikannimellä';
        searchElement['placeholder'].disabled = false;
        
        // URL parameters
        if(URLParams['x'] && URLParams['y']) {
            let searchItem = {
                label: URLParams['name'],
                geometry: [parseFloat(URLParams['x']), parseFloat(URLParams['y'])]
            };
            handleSearchItem(searchItem);
        }

        // let errorMessage = jQuery.i18n('notification_problemsLoadingEnvironment');
        let errorMessage = 'Errors when loading some data:';
        let errors = false;
        for (let i=0; i<successArray.length; i++) {
            if(!successArray[i].success) {
                errors=true;
                errorMessage = errorMessage + ' [' + successArray[i].target + ']';
            }
        }
        if(errors) {
            alert(errorMessage);
        }
    }).catch(function(exception) {
        alert('Failed to load datasets');
        console.log(exception);
    });


    // autocomplete action
    //jQuery.support.cors = true;
    jQuery("#searchField").autocomplete({
        source: getPlaceNames,
        select: function (event, ui) {
            handleSearchItem(ui.item);
        },
        response: function (event, ui) {
            jQuery("#searchField").autocomplete("option", "hitCount", ui.content.length);
        },
        hitCount: 10,
        delay: 500,
        close: function() {
            jQuery('#searchField').autocomplete('close');
        }
    }).data('uiAutocomplete')._renderItem = function (ul, item) {
        if(item.icon) {
            return jQuery('<li />')
                .data('item.autocomplete', item)
                .append('<a>' + item.label + '</a><img style="margin-left:5px;" src="' + item.icon + '" />')
                .appendTo(ul);
        } else {
            return jQuery('<li />')
                .data('item.autocomplete', item)
                .append('<a>' + item.label + '</a>')
                .appendTo(ul);
        }
    };

}



function readURLParameters() {
    var queryDict = {};
    try {        
        location.search.substr(1).split("&").forEach(function (item) {
            if (item != undefined && item != "") {
                var param = item.split("=")[0];
                // replace empty space codes with empty space and store
                queryDict[param] = item.split("=")[1].split('%20').join(" ");
            }
        });
    } catch (error) {
        console.error('readURLParameters: ' + error.message);
    } finally {
        return queryDict;
    }
}


function initiateMMLBasemap(req) {
    
    var wmtsParser = new ol.format.WMTSCapabilities();
    var capabilities = wmtsParser.read(req.response);
    var WMTSoptions = ol.source.WMTS.optionsFromCapabilities(capabilities, {
        layer: 'taustakartta',
        matrixSet: 'ETRS-TM35FIN',
        requestEncoding: 'REST'
    });
    WMTSoptions.attributions = 'Taustakartta © <a href="https://www.maanmittauslaitos.fi/">Maanmittauslaitos</a>';
    WMTSoptions.urls[0] = sykeProxy + '?' + WMTSoptions.urls[0];
    // console.log(WMTSoptions.urls[0]);

    basemap = new ol.layer.Tile({
        opacity: 1,
        source: new ol.source.WMTS(WMTSoptions),
        minResolution: 1,
        maxResolution: 2000
    })
}




function handleSearchItem(searchItem) {
    
    let name='';
    if (searchItem.MMLPlaceID) {  // from MML service
        theSearchItem.coordinates = [parseFloat(searchItem.coordX), parseFloat(searchItem.coordY)];
    } else {   // location from monitoring points
        theSearchItem.coordinates = searchItem.geometry;
    }
    theSearchItem.label = searchItem.label;
    updateMap()
    updateWaterShedInfo(theSearchItem.coordinates);
    calculateNearByPoints();
}


// function updateMap(coordinates, placename, selectedTheme) {
function updateMap() {

    //get elements & values
    let placename = theSearchItem.label;
    let coordinates = theSearchItem.coordinates;
    let selectedTheme = document.getElementById("selectTheme").value;
    let mapElementId = 'mapDiv1';
    let mapElement = document.getElementById(mapElementId);
    let mapTitle = document.getElementById('mapTitle1');

    //reset old
    mapElement.innerHTML = '';
    mapTitle.innerHTML = placename;

    //layer based on the select map theme
    let layer = new ol.layer.Tile({
        opacity: 0.7,
        source: new ol.source.TileWMS({
            url: forecastUrl,
            params: {
                'LAYERS': mapLayers[selectedTheme].layer,
                'TRANSPARENT': 'TRUE',
                'FORMAT': 'image/png'
            },
            serverType: 'geoserver',
            // Countries have transparency, so do not fade tiles:
            transition: 0
        })
    })

    let newMap = new ol.Map({
        target: mapElementId,
        // controls: [attribution],
        interactions: ol.interaction.defaults({
            doubleClickZoom: false,
            dragAndDrop: false,
            dragPan: false,
            keyboardPan: false,
            keyboardZoom: false,
            mouseWheelZoom: false,
            pointer: false,
            select: false
        }),
        controls: [],
        layers: [
            basemap,
            layer
        ],
        view: new ol.View({
            projection: 'EPSG:3067',
            center: coordinates,
            zoom: 8
        })
    });
    // location on map
    symbology_addSelectedLocation(newMap, coordinates);
    // point observations
    addObservationsToMap(newMap, mapLayers[selectedTheme].pointObservationType)
}


function selectTheme_changed() {
    if(theSearchItem && theSearchItem.coordinates) {
        updateMap();
        updateNearbypointsSelect();
    }
}


function selectNearbypoint_change() {
    updatePikanakyma();
}


function selectNearbypoint2_change() {

    // FIRST: UPDATE THEME/OBSERVATION TYPE SELECTOR
    
    // clear dropdown element
    let selectObsType = document.getElementById('selectObsType');
    for (a in selectObsType.options) { selectObsType.options.remove(0); }

    // get monitoringpoint
    let monitoringpoint;
    for(let i=0; i<monitoringPoints.length; i++) {
        if(monitoringPoints[i].id==document.getElementById('selectNearbypoint2').value) {
            monitoringpoint = monitoringPoints[i];
            break;
        }
    }
    if(!monitoringpoint) {
        return;
    }

    let suureet = monitoringpoint.suure;
    for (var i = 0; i < suureet.length; i++) {
        let option = document.createElement("option");
        option.label = jQuery.i18n(suureet[i]);
        option.value = suureet[i];
        selectObsType.add(option);
    }

    selectObsType_change();
}


async function selectObsType_change() {
    
    // observation type
    let selectObsType = document.getElementById('selectObsType');
    // get monitoringpoint
    let monitoringpoint;
    for(let i=0; i<monitoringPoints.length; i++) {
        if(monitoringPoints[i].id==document.getElementById('selectNearbypoint2').value) {
            monitoringpoint = monitoringPoints[i];
            break;
        }
    }
    if(!monitoringpoint) {
        return;
    }

    // load minute observations and then draw the graph
    minuuttihavainnot = [];
    minuuttiennuste = [];
    minuuttihavaintojenloppu = undefined;
    let correctProduct = selectObsType.value=='mw' ? 'sw' : selectObsType.value;
    let minHavUrl = 'https://wwwi2.ymparisto.fi/i2/minhavhtml/' + monitoringpoint.id + '_' + correctProduct + '.js';
    let minuteObservationPromise = new Promise((resolve,reject) => {
        try {
            jQuery.ajax({
                url: minHavUrl,
                dataType: "script",
                timeout: 5000}).done(function (script, textStatus) {
                    resolve(true);
                })
                .fail(function (jqxhr, settings, exception) {
                    resolve(false);
            });  
        } catch (err) {
            resolve(false);
        }
    });
    // try {
        //wait, but no need to change behaviour based on result
        result = await minuteObservationPromise;
        drawChart(monitoringpoint.name, monitoringpoint.id, correctProduct, 'divGraafi', $.i18n().locale);
    // } catch (err) {
    //     console.error('Failed to draw ' + correctProduct + ' graph for ' + monitoringpoint.name);
    // }
}



function calculateNearByPoints() {
    
    let searchUnits = 'kilometers';
    let selectedCoords = proj4('EPSG:3067', 'EPSG:4326', theSearchItem.coordinates);  //coords in WGS84

    // loop monitoring points:
    for (var i = 0; i < monitoringPoints.length; i++) {
        let targetCoords = proj4('EPSG:3067', 'EPSG:4326', monitoringPoints[i].geometry);
        // calculate distance
        let distance = turf.distance(
            turf.point(selectedCoords),
            turf.point(targetCoords),
            { units: searchUnits }
        );
        // store it in monitoring point list
        monitoringPoints[i].distance = distance;
    }

    // order by distance
    monitoringPoints = monitoringPoints.sort(sortByDistance);

    // update nearbypoints select element
    updateNearbypointsSelect();
}
function sortByDistance(a,b){
    if (a.distance < b.distance) {
        return -1;
    } else {
        return 1;
    }
}


function updateNearbypointsSelect() {
    
    // clear dropdown element
    let selectNearbypoint = document.getElementById('selectNearbypoint');
    for (a in selectNearbypoint.options) { selectNearbypoint.options.remove(0); }
    let selectNearbypoint2 = document.getElementById('selectNearbypoint2');
    for (a in selectNearbypoint2.options) { selectNearbypoint2.options.remove(0); }

    let selectedMapLayer = mapLayers[document.getElementById("selectTheme").value];

    for (var i = 0; i < monitoringPoints.length; i++) {
        let suureet = monitoringPoints[i].suure;
        let intersection = suureet.filter(suure => selectedMapLayer.monitoringPointType.includes(suure));
        if(intersection.length>0 && selectNearbypoint.options.length<5)  {
            // first select (pikanäkymä)
            let option = document.createElement("option");
            option.label = monitoringPoints[i].name.trim() + ', ' + monitoringPoints[i].distance.toFixed(1) + ' km';
            option.value = monitoringPoints[i].id;
            selectNearbypoint.add(option);
            // second select (graafit)
            let option2 = document.createElement("option");
            option2.label = monitoringPoints[i].name.trim();
            option2.value = monitoringPoints[i].id;
            selectNearbypoint2.add(option2);
        }
    }
    updatePikanakyma();
    selectNearbypoint2_change()

    // show
    document.getElementById('nearbypoints').style.display='flex';
    document.getElementById('nearbypoints2').style.display='flex';
}


function updateSelectObsType() {
    
}

function updatePikanakyma() {
    
    // get monitoringpoint
    let monitoringpoint;
    for(let i=0; i<monitoringPoints.length; i++) {
        if(monitoringPoints[i].id==document.getElementById('selectNearbypoint').value) {
            monitoringpoint = monitoringPoints[i];
            break;
        }
    }
    if(!monitoringpoint) {
        return;
    }

    // get variable    
    let selectedTheme = document.getElementById("selectTheme").value;
    let selectedVariable = mapLayers[selectedTheme].monitoringPointType[0];    

    // construct URL to the JSON
    let baseUrl = monitoringpoint['kuva'].substr(0, monitoringpoint['kuva'].lastIndexOf("/") + 1);
    let JSONurl = baseUrl + 'pikanakyma_' + selectedVariable + '.json';

    //ensure correct urls (with https)
    if (JSONurl.startsWith('http://')) {
        JSONurl = 'https://' + graph.slice(7);
    } else if (!JSONurl.startsWith('https://')) {
        JSONurl = 'https://' + JSONurl;
    }

    // make request
    let JSONrequestPromise = getDataObj(JSONurl);
    JSONrequestPromise.then(function (result) {
        if (result != null) {
            var JSONdataObj = JSON.parse(result);
            if (JSONdataObj.aikaleima != '' && JSONdataObj.aikaleima != undefined && JSONdataObj.taso != '' && JSONdataObj.taso != undefined) {
                
                // check for time being recent enough
                var dataDate = new Date(JSONdataObj.aikaleima);
                var currentDate = new Date();
                var diff = Math.floor((currentDate - dataDate));
                if(diff < 24*60*60*1000) {
                    teePikanakyma(selectedVariable, theSearchItem.label, JSONdataObj, 'divPikanakyma', JSONurl);
                    return;
                }
            }
            document.getElementById('divPikanakyma').innerHTML="Ei ajantasaisia tietoja";
        }
    }).catch(function (exception) {
        console.error(exception);
        document.getElementById('divPikanakyma').innerHTML="Ei ajantasaisia tietoja";
    });

}


function updateGraafi() {
    
}


function updateWaterShedInfo(coordinates) {
    // query watershed area ID
    //http://kkxgsmapt1:8080/geoserver/vespa/wfs?service=WFS&request=GetFeature&version=1.0.0&typeName=vespa:paavesistoalueet_rannikkopuskuri&outputFormat=JSON&propertyName=vespa:ORIG_FID&CQL_FILTER=INTERSECTS(geom,POINT(275968%206725120))
    //https://geoserver2.ymparisto.fi/geoserver/vespa/wfs?service=WFS&request=GetFeature&version=1.0.0&typeName=vespa:paavesistoalueet_rannikkopuskuri&outputFormat=JSON&propertyName=vespa:ORIG_FID&CQL_FILTER=INTERSECTS(geom,POINT(275968%206725120))
    let queryURL = 'https://geoserver2.ymparisto.fi/geoserver/vespa/wfs?service=WFS&request=GetFeature&version=1.0.0&typeName=vespa:paavesistoalueet_rannikkopuskuri&outputFormat=JSON&propertyName=vespa:ORIG_FID' +
                    "&CQL_FILTER=INTERSECTS(geom,POINT(" + coordinates[0] + '%20' + coordinates[1] + '))';
    let req = new XMLHttpRequest();
    req.open('GET', queryURL)
    req.timeout = 2000
    req.onload = function () {
        if (req.status === 200) {
            showAreaInfo(req);
        } else {
            console.error('Failed to get water shed area ID from ' + queryURL);
        }
    }
    req.onerror = () => {
        console.error('Failed to get water shed area ID from ' + queryURL);
    }
    req.send();

    // ELY
    /*
    http://paikkatieto.ymparisto.fi/arcgis/rest/services/LAPIO/LAPIO_Hallrajat/MapServer/2/query?where=1%3D1&geometry=234551%2C6841090&geometryType=esriGeometryPoint&inSR=EPSG%3A3067&spatialRel=esriSpatialRelIntersects&outFields=ELYyNro%2CELYyLyhenne%2CELYyNimi%2C+ELYyNimiRuo&returnGeometry=false&f=json
    */
    let elyUrl = 'http://paikkatieto.ymparisto.fi/arcgis/rest/services/LAPIO/LAPIO_Hallrajat/MapServer/2/query?where=1%3D1' +
                '&geometry=' + coordinates[0] + '%2C' + coordinates[1] + '&geometryType=esriGeometryPoint&inSR=EPSG%3A3067' +
                '&spatialRel=esriSpatialRelIntersects&outFields=ELYyNro%2CELYyLyhenne%2CELYyNimi%2C+ELYyNimiRuo&returnGeometry=false&f=json';
    let req2 = new XMLHttpRequest();
    req2.open('GET', elyUrl)
    req2.timeout = 2000
    req2.onload = function () {
        if (req2.status === 200) {
            showELYInfo(req2);
        } else {
            console.error('Failed to get ELY area from ' + elyUrl);
        }
    }
    req2.onerror = () => {
        console.error('Failed to get ELY area from ' + elyUrl);
    }
    req2.send();
}


function showAreaInfo(req) {
    // console.log(req.response);

    let response = undefined;
    try {
        response = JSON.parse(decodeURIComponent(escape(req.response)));
    } catch (error) {
        try {
            response = JSON.parse(req.response);
        } catch (error) {
            console.log('GetFeatureInfo response error: ' + error.toString());
            return;
        }
    }

    // fetch attributes
    if (response['features'] != null) {
        if (response['features'].length > 0 && response['features'][0]['properties'] != null) {
            properties = response['features'][0]['properties'];
        } else {
            //no features found
            properties = null;
            // return;
        }
    } else if ((Array.isArray(response)) && (response.length > 0)) {
        properties = response[0]
    }
    if (properties != null) {
        let propertyNames = Object.keys(properties);
        if (propertyNames.includes('ORIG_FID') && properties['ORIG_FID']!=null && properties['ORIG_FID']!=undefined) {
            switch (properties['ORIG_FID']) {
                case 0:
                    document.getElementById('divAluenimi').innerHTML = 'Lappi';
                    break;
                case 1:
                    document.getElementById('divAluenimi').innerHTML = 'Oulu-Koillismaa ja Kainuu';                    
                    break;
                case 2:
                    document.getElementById('divAluenimi').innerHTML = 'Pohjanmaa';                    
                    break;
                case 3:
                    document.getElementById('divAluenimi').innerHTML = 'Etelä- ja Lounais-Suomi';                    
                    break;
                case 4:
                    document.getElementById('divAluenimi').innerHTML = 'Kanta-Häme, Pirkanmaa ja Satakunta';                    
                    break;
                case 5:
                    document.getElementById('divAluenimi').innerHTML = 'Keski-Suomi, Päijät-Häme ja Kymenlaakso';                    
                    break;
                case 6:
                    document.getElementById('divAluenimi').innerHTML = 'Itä- ja Kaakkois-Suomi ';                    
                    break;
                default:
                    break;
            }
        }

    }
}


function showELYInfo(req) {
    let response = undefined;
    try {
        response = JSON.parse(decodeURIComponent(escape(req.response)));
    } catch (error) {
        try {
            response = JSON.parse(req.response);
        } catch (error) {
            console.log('GetFeatureInfo response error: ' + error.toString());
            return;
        }
    }

    // fetch attributes
    if (response['features'] != null) {
        if (response['features'].length > 0 && response['features'][0]['attributes'] != null) {
            properties = response['features'][0]['attributes'];
        } else {
            //no features found
            properties = null;
            // return;
        }
    } else if ((Array.isArray(response)) && (response.length > 0)) {
        properties = response[0]
    }
    if (properties != null) {
        let propertyNames = Object.keys(properties);
        if (propertyNames.includes('ELYyNro') && properties['ELYyNro']!=null && properties['ELYyNro']!=undefined) {
            //ELYyNro,ELYyLyhenne,ELYyNimi, ELYyNimiRuo
            document.getElementById('divELYtiedot').innerHTML = 'ELYyNro: ' + properties['ELYyNro'] + '<br/>' +
                'ELYyLyhenne: ' + properties['ELYyLyhenne'] + '<br/>' +
                'ELYyNimi: ' + properties['ELYyNimi'] + '<br/>' +
                'ELYyNimiRuo: ' + properties['ELYyNimiRuo'] + '<br/>';
        }
    }
}


symbology_addSelectedLocation = function (map, location) {

    let vectorSource = new ol.source.Vector();

    let iconStyle = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
            // src: FEATURE_LAYER_LIST[layerID].marker,
            src: './img/currentPoint_purple.png',
            opacity: 1,
            anchor: [0.5,1]
        })), 
        zIndex: 100
    });

    let selectionLayer = new ol.layer.Vector({
        title: 'valitutpaikat',
        source: vectorSource
        // , style: styleFunction
        , style: iconStyle
        , zIndex: 10001
    });
    //map.addLayer(vectorLayer);
    let g = new ol.layer.Group({
        title: 'valitutpaikat_group',
        nested: true,
        layers: [
            selectionLayer
        ]
    });
    map.addLayer(g);
    
    selectionLayer.getSource().clear();
    let point = new ol.geom.Point(location);
    let feature = new ol.Feature({name:'valittu paikka', geometry: point});
    selectionLayer.getSource().addFeature(feature);
}


// function addObservationsToMap(map, observations, obsType) {
function addObservationsToMap(map, obsTypes) {
    
    let vectorSource = new ol.source.Vector();
    let vectorLayer = new ol.layer.Vector({
        title: 'rasterData',
        source: vectorSource,
        zIndex: 10001,
    });

    let styleFunction = function (feature) {
        var zoom = map.getView().getZoom()+1;
        var font_size = 10 + zoom;
        return[
            new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 3,
                    stroke: new ol.style.Stroke({
                        color: '#3399CC'
                    }),
                    fill: new ol.style.Fill({
                        // color: '#3399CC'
                        color: '#fff'
                    })
                    }),
                text: new ol.style.Text({
                    offsetY : 10,
                    font: font_size + 'px Calibri,sans-serif',
                    fill: new ol.style.Fill({ color: '#000' }),
                    stroke: new ol.style.Stroke({
                        color: '#fff', width: 2
                    }),
                text : feature.get('value')+" "+feature.get('unit')//+" "+feature.get('lastObs')
                })
            })
        ]
    }    
    vectorLayer.setStyle(styleFunction);

    // add data
    for(let i=0; i<obsTypes.length; i++) {
        let obsType = obsTypes[i];
        let observations = pointData[obsType].features;
        for (var j = 0; j < observations.length; j++) {
            var tmpFeature=observations[j];
            var pvm = new Date(tmpFeature.viimeisinHavainto.trim());
            var d = new Date();
            d.setDate(d.getDate() - 1); //only show obs from the last 24 hours
            if(pvm>d) {
                var arvo = tmpFeature.viimeisinArvo;
                var yksikko=""
                var point = new ol.geom.Point([tmpFeature.geometry[0], tmpFeature.geometry[1]]);
                switch (obsType) {
                    case 't2mPoints':
                        yksikko = '\xB0' + 'C';
                        break;
                    case 'icePoints':
                        yksikko = "cm";
                        arvo = Math.round(arvo);
                        break;
                    case 'lakeTempPoints':
                        yksikko = '\xB0' + 'C';
                        break;
                    case 'fmiTWATERPoints':
                        yksikko = '\xB0' + 'C';
                        break;
                    case 'snowDepthPoints':
                        yksikko = "cm";
                        arvo = Math.round(arvo);
                        break;
                    case 'swePoints':
                        arvo = Math.round(arvo);
                        yksikko = "kg/m2";
                        break;
                    case 'fmiPrecPoints':
                        yksikko = "mm";
                        break;
                    case 'fmiWATLEVPoints':
                        arvo=Math.round(arvo/=10.0);
                        yksikko = "cm";
                        break;
                    case 'groundFrostPoints':
                        var tmp = arvo.split("/");
                        if (tmp[0].length>0){
                            arvo=Math.round(tmp[0])
                        }
                        else{
                            arvo="";
                        }
                        if (tmp[1].length>0){
                            arvo=arvo + "/" + Math.round(tmp[1])
                        } else {
                            arvo=arvo + "/-"
                        }
                        yksikko = "cm";
                        break
                    default:
                        break;
                }
                var tmp = new ol.Feature({
                    geometry : point,
                    value: arvo,
                    lastObs: tmpFeature.viimeisinHavainto,
                    unit : yksikko
                });
                vectorLayer.getSource().addFeature(tmp);
            }
        }   
    }

    map.addLayer(vectorLayer);
}
